package mx.mobilecard.satservicios.controller;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import mx.mobilecard.satservicios.utils.AddCelGenericMail;
import mx.mobilecard.satservicios.model.vo.DatosCorreoVO;
import mx.mobilecard.satservicios.model.vo.DatosReciboVO;
import mx.mobilecard.satservicios.model.vo.ProcomRespuestaVO;
import mx.mobilecard.satservicios.model.vo.RequestPagoMovilesVO;
import mx.mobilecard.satservicios.services.MailSenderService;
import mx.mobilecard.satservicios.services.SatService;
import mx.mobilecard.satservicios.utils.UtilsService;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

/**
 * Handles requests for the application home page.
 */
@Controller
public class SatController {
	
	private static final Logger logger = Logger.getLogger(SatController.class);
	private static String imagenBaseEmailStr="/usr/java/resources/images/SAT/BaseCorreo.png";
	private static String imagenBancoEmailStr="/usr/java/resources/images/SAT/multiva.png";
	private static String imagenBaseMovilStr="/usr/java/resources/images/SAT/BaseMovil.png";
	private static String imagenBancoMovilStr="/usr/java/resources/images/SAT/bancoLogoMovil.png";
	private static final String patronCom = "dd/MM/yyyy HH:mm";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	@Autowired
	private SatService satService;
	@Autowired
	private MailSenderService mailSenderService;
	@Autowired
	private UtilsService utilService;	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}."+ locale);				
		return "home";
	}
	

	@RequestMapping(value = "/envia3Ds", method = RequestMethod.GET)
	public String envia3Ds(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}."+ locale);				
		return "home";
	}
	
	
	@RequestMapping(value = "/pruebas", method = RequestMethod.GET)
	public @ResponseBody String pruebas() {
		logger.info("Iniciando las pruebas");
		StringBuilder respuesta=new StringBuilder();
		String jEnc=null;
		HashMap parametros;
		String json="{\"email\":\"vfernando.lira@gmail.com\",\"firmaBase64\":\"/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/7AARRHVja3kAAQAEAAAAPAAA/9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAggC+AwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/fyiiigAooooAKKKKACiiigAooooA+XP+CuP/BRfTf8Agl1+xD4m+KV3a2+qa1HPFpXhzS7h3SPVdSm3eVEzIp2qkaSzMflysBXcGIrtf+CeP7RXiP8Aa0/Yo+GfxK8WeGV8IeIvGWiw6nfaUgk8qFnzh4t43+XIuJk3FvlkHzN98/mL/wAFofBV3/wVu/4LZfAv9kvTpHm8E/DWwbxl8Q5rTzFa0jnaN5IpTtZVY20dpHE23b5mqDd/s/stoOg2PhPQ7PTdNs7fT9O0+FLa2traJYobWJF2oiIvCqq4UKvtQBpUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFcn8avizpfwG+Dvizxxrn2gaL4N0e713Ufs8e+X7NawtPLsUkbm2IcetdZX49f8AB3d+3pY/Cb9kXR/gHo+sRW3jT4u31rcalE42raaHBMzGR3/h8y6iiUbeqxz/APAgDX/4NePgrqvxT0P4xftceNoZpvGnx48S3UdhcTWH2YLp0U253iPAaOWf5flUBfsY61+t1eGfsIeHvhf8FP2cvBPwr+Gvi7wx4k07wPoUFlD/AGbqUN1JOsaqHuWEbvt8x33n3l613X7Qvx78M/sv/A7xZ8QvF10dP8N+C9Ln1bUJfl3GOFN2xAzANI/CIufmYhaAO5or+YnwB/wUI/ag8Kft0fBP9t34ja1caT8M/jt4pfwrp/h+1v8AzopPDkN55Fzax2ZYKscXzsjSbWkn/ffx76/db4Y/8FPvCPxd/wCCnfjr9mPQ9NvrrXfh74WXxHqusrOjWYlM1sjWYUZO9Fuoctu+9vXb8maAPqCvOP2fP2pvh/8AtXaT4kv/AIdeKdP8Waf4S1658MardWW/yYdQtxG0sKuwAk2rKn7yPcjZ+VjXEf8ABUX9o/8A4ZJ/4J5/GL4hRz3VreeHfC941hPbHbLFeyx+RasrZGMXEsXPavgH/gy2LH/glx4+kb5mb4qai271/wCJTpFAH6+V5z+1R+0r4T/Y6/Z88V/FDxveSWPhbwbZNe30kcRkkf5lRI0QfekeRkjUf3nHSvRq/D3/AIPYvjhrPhb9nv4I/D2zk2aP421rVNUvxj5pHsI7ZIVz/dzfP+QoA8M1b/g6l/aY8LftG+Dfih4g+HI0H9m3xxqU0WjaRPpJVNWsoZfIuJLfUmQNNdQ7vn2fu/M/h21/RrX8wf8AwcS6i9l8PP2GfgToul2dvHoPw107V4BaQ7HnudQ8m3fhfl5ezL/d+9I9f0+UAFFFFABRRRQAUUUUAFFFFABXO/Eb4n+G/g74Mv8AxJ4u8QaL4V8O6YglvdV1e9jsbO1UsqhpZ5CqRjcQvzHuK6KvzL/4OEf+CJnxE/4K5D4dzeAPHuj+Hz4OF0l3pPiG8uU0ufzNpS4iWCGQ/aOGVmb+HYKAPn/9v3/g8g8B/CrUdT0H4B+Ef+FialB+5i8R608lno4kWXazJbrie4jKDht8XUV+dOs/8FW/+Cj3/BRLwj4g8Q+E/E3xQl8J+FVkvr+bwFo66RZ6THtLsJbq1RJWULGW/eSt0euF/wCCUX/BGbVP26/+Cl+ofCHXL2FvBfw8vrmTxrr2hXMd1b+RbSbDFaz7tjtcS7IkkXdtWQy7X2Yr+o/4wfCLwr+zv/wT78ceE/B+g6b4b8K+F/A+p2un6dZRiK3tIVs5zjHqfvMx+ZmJZuaAP5gv+Ce/wQ/bO/4LefELVPDmgfHL4hahY+C7ZbnUdW8X+N9VmsNJ+0bvKj4aV/MmaDgKv/LMn+Cpv+CZH/BC/wAdf8FVP2qviZ4R1T4iWuiab8MppbPW/F0Vu3iO2urpJmijit5Fmjjm37HZW83b5Y3Lur3L/g3t/aVuv2L/APglV+3t8UrGSa11TSNF0Cy0y4R/LeC+nXULW1kVv9ie6iav01/4NKv2W4fgP/wSl0zxjcW0cetfF3WLvXbiQM3mfZoZGs7ZWB4/5YPINv8Az8UAYP7An/BpP8Ef2PPi9pPjjxh4p8QfFnWNBnju9NtL20j03SYbiNt6zSwIzvNtZV2q0vl/3keuM/4PBv2ntW8O/s9/C34IaJeeS3xc10y6vDBdlLl7W1kh8mN4w3zQyTy7vmGN1sP7tfstX4B/8FpfEuk/Gv8A4OmP2W/BtxD9usfDD+GdP1K0uoFlt5pJNUnvGXa2dytBLCrbqANb/g5A+HOleANc/YB/Zz8OzXWral4bvINJtLeK3f7RcQRjTbC1ZQueZGicbVLHiuB/4JD/ABPsPin/AMHXvxo8QaTdfbtL1y78StZ3CbkE0Ib5Gw2D91KP+C7X7UNvcf8ABx18KriayKaf+zbY6Tr2pFrjb9tjtpP7bl2/L8n7l0j/AIq+bf8Ag1l8dw6j/wAFyPD93NN+/wDEem69sL/emdreef8A9BQ0Afo9/wAHlH7Xn/CsP2M/BfwhsrySPUPibrB1DUY7e92uNPsdr+XNEPmaOW4kiZSfl3Wn/fPRf8GXiKP+CVfjPb95vidqO76/2XpVflH/AMFnfi943/4LS/8ABQH4zePPhfot54t+HHwH8N+Ut1ageVaaJZSnz775gpcSXMtxNgAyeSP7sT7frv8A4M4P+CinhH4XDxt+zv4u1q10TUvFmtJ4i8J/adscWp3jwx211a+af+WzLb2hjj/i/ebfm4YA/oXr8CP+D3D4JX09n8CfiNa2epTafCdU8OaldbN9rZyHyJ7WPOPlkl/0vv8AMIP9iv3X+InxI8O/CHwbfeIvFWvaL4X8P6cqteapqt7HZ2dqpYIGklkKovzFRlmHUV/OD/wcWf8ABfjwd/wUf8K2PwK+Dej/ANreDbTW4r+98U6pb+S+pXcRkiiWyjb5o4MS5M0myR87dqrkygHjP/BFnTPGv/BV/wD4LbfCvxT4u1izguPh3Dpmuzyp5ca/ZdDt4IrW3iiZ13SSNFDv8v5v9fLt+Rq/ob8O/wDBYf4D+N/28dH/AGdfDPipvF3xA1W3uZ2l0RFvtKsWht5Ll4Z7pG2LJ5cR+Vd2GO1sNX4sfBv/AIMsfjN4iudFuPG3xN+HvhvT7xYZNSt7CO6vtQsVO0uiqUWGSROV/wBZtP8AexX7Lf8ABLn/AIIz/Bz/AIJP+FbyLwFY3OreMNato7fWfFOrbJNRv1UKTEm0BYLfzF3+SnU7NzPsQgA+uqKKKACiiigAooooAKKKKACvzb/4OMf+CrF5+wT+zJb/AA/+H9xet8bvi+r6V4dGmTr9t0WBmVJb7aMyLI27y4Sq/wCsJZW/dGvt79qb9pXwr+xz+z14q+J3je9ksfC/g2xa9vpI08yR/mVEjRf4pHkZEUd2cV+J/wDwTb0fVf2t/jV8bf8AgqJ8efDuvXHhPwDbX+pfDrw19kF4skNlDKqtFiL5ls40SIXCxKv2nz52ZPJegD9LP+CIn/BMew/4Ja/sQaN4RnS3m+IHiIRa140vvLj3PqEka7rVZEz5kNt/q0O7DHfJ8vmkV6F/wVg+K9j8E/8Agmf8dPEWoGRbe18Fapaq0X3vOuIGt4vT/lrKma0v+CdH7cvhf/go5+yH4S+LXhSM2Nr4ihK3ulyTpcTaLext5c9q7J12uPlYqpaMo+1d1fHP/B2X+0hH8D/+CS+s+HI3s5NQ+KGtWXh9IZJcTLbo32yaVF3fNt+zxRnt+/8A9oUAfhR4FurT4df8G+fi5jPcf2l8WPjbaaOIxAvlR2+kaWtyzM+7ndJfphdv/LM1/Vj/AME+PCMfgD9gn4I6DA3nRaL4A0HT0k/vrFp0CBvx21/Oh/wUR/Yy1j4Jf8G2X7J2uf2A2mtN4k1LXfELvKytNPqiyNYy+Wzfx2kEP3V2/IP7/wA/7Wf8G937cGh/ttf8Eu/hrLBqlnc+LPh/pFt4S8TafDI3n2FxaL5EEkqn/nvBFFNuX5MyOvVGVQD7lr+Yf/gs7+0Dp37Jv/B0xZ/ErxRDqNx4d8Fax4W1m6SxjWS5e1hsLQyiJWZVZvv4+Za/pq1fV7Xw7pVxe3txb2dnZxNNPPM4jjgjUZZmY8KoHev5iv8Agumtn/wW0/4LEWvh39lnQbr4lah4d8OW2ga3rGjwJ/Z15PDeyiS8a6HyG1i+0QQ/apMK3l/KWj8p3APz0/bX/aEvv21v24fiV8QtMt9S3fETxTe3umWRJkukt7i4b7PbfJ95hGyR7a+mf2df+DcH9sr4l/GnSfD7fDLWPh7HcQRXVz4j1i+jtbHTrWeNclpI5GdpPLfa9vGGlXJVlXDV+0//AAQ4/wCDcLwf/wAE6tE0r4h/FG203xr8cHdbyGT5pdP8H/Iw8m1H3Zp/my9w44YJ5QXaXl/UDV9XtfDulXF7e3FvZ2dnE0088ziOOCNRlmZjwqgd6APm/wD4Jx/8Er/hj/wTY/ZUX4W+GtNt9ej1iJj4q1TU7VHl8VXDx7JWnQ5XydpKJb/cWPj52Lu/5B/thf8ABmT48/4XPc6t8AviV4KtvCt3M91BYeLJ7yxvNGYyMywxzW8E/nKq4/eN5bf7NYv/AAWL/wCDrX4jL+0x/wAIv+yz4rsfDngXwkzwXOvto9pqT+K7jd87ILqKQR2qbdqbFV3+di2Cir+xn/BHn/gpBY/8FUv2FvDfxRjtbHSfESzTaL4o0u0MjwabqkG3zEjL/wDLOSJ4Z0Xc+1LhFZ2ZGoA/mB/4KQ/8E2fjp+xt+0Z4F+DPjjx9ovxQ8ceNkt/7L0fw7ruoas0Mk9x5FtA6XMMTLJJJ9xdvNd1/wXZ/4Jl+Gf8AgnL+0t8DfhP4JhkuvEl98ONMu/EFwk8kp1fWptQvoJLmPdjarvGkaKqrhY0/i3NXtGmft4fCn4l/8HH3j/8AaE+Mfi7TW+HPwj1DUtR8PRrbzPceIW0+NrXSbeziRRuuPM8q4XzDGuYv3jev17/wRU/Y38Z/8FX/ANvHXv29fj3pdxp+mrfxv8ONE8gx287Qfuoroc7vJtVjVU+X99OXk3fuvnAP2I/Za8Ha18Pf2Y/h14f8Q/8AIwaF4Y03T9UO8P8A6VDaxxy/MMhv3itzXf0UUAFFFFABRRRQAUUUUAFFFfmP/wAHLH/BXqT/AIJ3fsw2/gTwJqU0Pxk+K0Mtvpc1ndKl14bsAyrPf7RmRZH+aG3Py/vPMdW3W+0gHx7/AMFWvjn4h/4L+f8ABVHwr+yH8JNS+0fCP4c351Dxh4g06Rp7aeSPy0vLx/mVJI7TzPs8P9+eWT5mV0x+vXxY/Yb0e6/4Jv8Aij9nv4fiLwjpd14Gu/CehSwzSWy2DyWrxRTSmHDN+8bzJevm5fdu3tn5n/4Nxv8AglCv/BOP9ji31zxdpP2X4vfErbqfiB7qPbeaZbdbbTmJY42Z8x+EbzJSrZ8pK/RigD+U/wD4N/f+Crs3/BGn9rfxX8O/jHa6xoPw/wDE922meJbeW2P2jwnq1tIYlupINhlKpiSGWNPmwd3ztEEP0v8AtmeOp/8Ag6J/4K0eB/hj8J57hvgR8GrOW+1jxLPb+VDOkskX2q6RHCu3nbLe3hhb5+Hkwiebs+9P+Cr3/BtV8Jf+CmPxDbx1pus33wt+IV7JH/aeqWFgl7Z6sqqw3T2pePM33f3iyDp8yvX0/wD8E5P+CcHw4/4Jk/s/2ngH4e6bt3Yn1fV50X7drl1tw087f+gR/dReBQBZ/bJ/4J/+A/2zf2INY+A+rWh0fwvdaXBp2kzW8Cyy6DJbKv2WaHf/ABQmNO/zLld3zGv5bfF3gf8Aat/4Nwf2wLq+sW1fwddvJNp9nrsdm114b8ZWoG75PMXyZ12uj+W37yFiPuNX9h9FAH83ej/H/wDb2/4Oa47LwPZ6bYfCn4H3QU69q+m6ddaboN+YtzMHuJGlmvGO9R9lify93ktIo2+bX7Z/8E0/+CZPwy/4Jb/AeDwT8O9N8y6uxHc69r11Go1HxDdKu3zJ37KuW8uFfkjBP8Tu7/SNFABX4J/8HY3/AAWguNGtpf2X/hfrFtuvIVl+IOpWkm6WBd26LSVbHyMdqyTbfm+5Hn/WpX6lf8FeP+Ci+mf8Eu/2G/E3xRuLe11HXUdNK8M6Zcl/L1PVZ1fyY5NmP3aKksz/ADLmO3fa27bX83v7Q37Ctzr/AIK/Z+8C6lO/jD9rn9q7X18ceJLm/naS88L6beFk06GVt2wteLPLdz+Y3mx/Z412pjdKAfnnd6XdWNvDNNb3EMd0m6JnQqsi9PlPev0M/wCCEn/BZuL/AIJUaT8b9P1ebVJLDxt4VnuPD0NpaR3McfiOCNvsTS7+VhcSOrt83RNy19EfHv8A4J92v/BV/wD4LJ6L+yj8O9YsfDPwx/Zd8Aw+Eb/xHY6Sn7trFN15cNbNJH5lxLql15D7W/56S/P8zP8AEX/BP7/gnPpf7Wdr+09fX+tXKaZ8Cfhrr/i2zuLeLcupXlor/Zo2+YbVfY7fxUAeu/8ABuh/wSktP+CoX7Z91qHjazmvPhn8PETV/EMQLLHq08jN9nsWdfurI29n6bo4pAuOtfo9+wf/AMHXmm/E39vyz+FPjDwb4B+GXwVvJ5tB8O6tBPLA2jGL5LL7Q3/Hutu/liL5Y4kj8xG3BEavI/8Ag0c8QzeF/wBjL9sy/s7iW1vtK0qwvoZ4pCskDpYaoyspHKsrLX5P/sKfsA/Ej/gp1+0o3w++GNnplxrEsMupXl5qE5gsNNtBMkb3E7qrMIw0qcKrPk/KpoA/sw/ae/aw+Hf7F/wmvfHXxQ8W6b4N8L2TLHJfXgZvMkO4rFFGitJLI21sRxqznBwK/Gr9qz/g9Y0HQ9blsPgr8JLzXbaGfJ1fxbf/AGNJ4wvzbLWDe/LdGaUfKPu8/LT/AGSf+DOvxXruuWNx+0t8ZP7a0HR2xbeHvCF9dXSzqVb/AJerxI/s+1tvypA24fxJUv8Awdbfst/Ab9ir/gm38K/A/wAO/APgrwT4g1LxwsumDT9OVL+Wwgs7v7Y73RBlkUTXNpu8yTrIn9zgA/Rr/gir/wAFY7H/AIK4/sy6h4y/4R2Pwn4k8O6l/ZGs6VHdNdQCTyldbiGRkX93Jl8Rtll8s7s8M32ZX5z/APBsL+xnffsi/wDBLHw1da1ayWXiL4l3k3i69imsfIntYZlSO1hJPzOvlRLMM/8APwfq36MUAFFFFABRRRQBi+M/EjeD/B2rawLHUNVbS7Oa7+w6fD5t5d+XGz+TAmRukfbtVcjLYr8A/wDgj3/wTO+Kn/BWf/gohqH7ZH7SGgrZ+CbfW5tU0vRdWS4265eRlltbe3id966fZNsw0jMjtAItsq+bs/oWooAKKKKACiiigAooooAKz9Z1mz8NaPc39/c29jZWMTXFxcXEixRQRou5ndm4VQvVu1aFfkX/AMHK8P7VH7T/AIp8Bfs4fAn4f+MLzwP4+s/tfinxRp8cg02dmmeJdPurkR7LWGNYvOfzJP3vnou35PmAPmfX/inpH/Bwf/wVg17x54wlmtf2Mv2S7C91i8ui919j1mG13S/aHXY3zXTReY8apHJ9jt9v+srnv+CbnxA8QfEm/wD2tP8Agpz8SEkjvvDen6jpfgBSBdR2F/NCttG0SvJny7aGW3tgsisrLcSfxJXc/wDBYL4S2v8AwSm/4JMfCP8AYg+EMa618RvjzrUH/CRNYz7r/XrjzLbzpFiC7wtzdC2gj+7+5t/L+f56/SS9/wCCQ3hhf+CNNx+yfpps9HjvPCUemXGqWSMqXesosc7X8uVJZZbyISP32fKuz5doB+Hf/BKT9tbS/wDgnd/wSb/ad+NX/CS6O/xq+Leuw+DPDVp56f2xazJbyTzahsVllSEfbTJ5i/L51vGG7V+lv/BBL/gkxP8ACn/giF4u0fWFi0/xr+1B4evbq/lnIH2HT7yxlttNhZkZ/l8iX7T93crXkkbLlK8I/wCCef8AwZyP8MPjdZ+Kf2gPGvhzxVoehXEVzZ+HfDIufI1Zl3Ni6nmWN441YJ8ka/N/fWv3eoA/jL/Ye/bS8df8EiPjT8XvDWreC76e68WeFtV8Aa9ot+ZLK6sbl1ZIp1+U/PDKq7l2/NGXXg4ev2y/4NM/+CV/iH9j74B+JPjB4+0u60fxd8U4ba30nT7qAxz2Gjx5dZH+bO65dlbay/KtvG38Zr9VvEXwV8HeMfGmneJNW8K+HNV8RaOV+w6pd6bDPe2Oxi6eVKyl48MxYbSOtdZQBg+PvFDeBvA2sa0mm6trT6PYzXo0/TYPPvr7ykaTyYIyw8yZ9u1FzyxFfi98L/8Agk18bf8AgtV/wUMh/aJ/az8I3Xwz+FPh91/4Rr4c3d20t9d20MjLFaypu3W8ZdPNuJGVGnL/ALuNFfen7f0UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRXyf/wAFpP22E/YE/wCCbnxM8fR30ll4gk06TRfDjQs0cv8Aal2jQ27qwU8xbjN24gPzd6APzh/Y8so/+CxH/BzL8S/i1ePcan8Mv2av+Jd4d23Bv9NnureRrS1kgmRlSNZZ0u79Nu7/AFf8X36/c6vzB/4NOv2TLP8AZ7/4JVaP4ykjmXxB8YNSutfvmmQq8MEU0lpaxdTuXy4PODYX/j5I7V+n1ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV+Ev/B6P8c5L/SvgP8H9O1eRZNe1G713VdMQNtkjDQ29nM3Y/O12q/jX7tV+G/8AwXu/4J/fEz9s7/gu5+ziND8C+N9U+HsehaNZ6z4ksvD91eaRo23Wb+a4M9wq+SjJD5bFXcHDpx8wyAfsl+zx8Hbf9nn9n/wN8P7G4a6s/Avh+w8P29wybWnitLeOBW25O3csY78ZrtqKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Z\",\"lineaDeCapturaSat\":\"0214 1X4U 9800 0627 9257\",\"monto\":\"18.00\"}";

		jEnc = AddcelCrypto.encryptSensitive("12345678",json);
		respuesta.append("<html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> </head> <body> <form METHOD=\"POST\" name =\"miforma\" id=\"miforma\" AUTOCOMPLETE=\"OFF\" ACTION=\"./inicio\"> <input type=\"hidden\" name=\"json\" value=\"");
		respuesta.append(jEnc);
		respuesta.append("\"> <input type=\"submit\" value=\"enviar\"> </form> </body> </html>");
	   
		return respuesta.toString();
	}
	
	@RequestMapping(value = "/inicio", method = RequestMethod.POST)
	public ModelAndView inicio(@RequestParam("json") String data) {
		RequestPagoMovilesVO requestPagoMovilesVO=null;
		ModelAndView mav=null;
		requestPagoMovilesVO=(RequestPagoMovilesVO)utilService.jsonToObject(AddcelCrypto.decryptSensitive(data), RequestPagoMovilesVO.class);
		mav = new ModelAndView("inicio");
		mav.addObject("email", requestPagoMovilesVO.getEmail());
		mav.addObject("firmaBase64", requestPagoMovilesVO.getFirmaBase64());
		mav.addObject("lineaDeCapturaSat", requestPagoMovilesVO.getLineaDeCapturaSat());
		mav.addObject("monto", requestPagoMovilesVO.getMonto());
		return mav;
	}
	
	@RequestMapping(value = "/realizaPago3Ds", method = RequestMethod.POST)
	public ModelAndView realizaPago3Ds(HttpServletRequest request) {
		ProcomRespuestaVO procomRespuestaVO=null;
		ModelAndView mav=null;
		DatosReciboVO datosReciboVO= null;
		String imagenBase64Movil=null;
		String imagenBase64Email=null;
		byte[] imagenByte=null;
		DatosCorreoVO datosCorreoVO=null;
		try{
			if(((Math.random()*1000)%9)!=0){
				//caso de exito
				datosReciboVO=new DatosReciboVO();
				datosReciboVO.setAutorizacion(String.valueOf(((int)(Math.random()*10000))+100000));
				datosReciboVO.setFecha(formatoCom.format(new Date()));
				datosReciboVO.setFirmaBase64(request.getParameter("param3"));
				datosReciboVO.setLineaCaptura(request.getParameter("param2"));
				datosReciboVO.setLlaveDePago("6EAD70B751");
				datosReciboVO.setMedioPresentacion("Internet");
				datosReciboVO.setMonto(request.getParameter("total"));
				datosReciboVO.setNumeroOperacion(String.valueOf((int)(Math.random()*1000000)));
				datosReciboVO.setNumeroTarjeta("XXXX-XXXX-XXXX-"+request.getParameter("cc_number").substring(12));
				datosReciboVO.setTipoTarjeta("Debito");
				datosReciboVO.setProducto("Crédito BBVA Bancomer");
				datosReciboVO.setSecuencia(String.valueOf(((int)(Math.random()*10000))+100000));
				imagenBase64Movil=generaImagenMovilExito(datosReciboVO);
				//imagenBase64Email=generaImagenEmail(datosReciboVO);
				imagenByte=generaImagenEmail(datosReciboVO);
				//logger.info("la imagen generada : "+imagenBase64);
				mav = new ModelAndView("exito");
				mav.addObject("imagenBase64", imagenBase64Movil);
				datosCorreoVO=new DatosCorreoVO();
				datosCorreoVO.setEmail(request.getParameter("param1"));
				datosCorreoVO.setImagenPago(imagenByte);
				datosCorreoVO.setReferenciaSat(request.getParameter("param2"));
				mailSenderService.enviarCorreo(AddCelGenericMail.generatedMail(datosCorreoVO));	
			}else{
				datosReciboVO=new DatosReciboVO();
				datosReciboVO.setAutorizacion("0000");
				datosReciboVO.setFecha(formatoCom.format(new Date()));
				datosReciboVO.setFirmaBase64(request.getParameter("param3"));
				datosReciboVO.setLineaCaptura(request.getParameter("param2"));
				datosReciboVO.setLlaveDePago("6EAD70B751");
				datosReciboVO.setMedioPresentacion("Internet");
				datosReciboVO.setMonto(request.getParameter("total"));
				datosReciboVO.setNumeroOperacion(String.valueOf((int)(Math.random()*1000000)));
				datosReciboVO.setNumeroTarjeta("XXXX-XXXX-XXXX-"+request.getParameter("cc_number").substring(12));
				datosReciboVO.setTipoTarjeta("Debito");
				datosReciboVO.setProducto("Crédito BBVA Bancomer");
				datosReciboVO.setSecuencia(String.valueOf(((int)(Math.random()*10000))+100000));
				imagenBase64Movil=generaImagenMovilError(datosReciboVO);
				mav = new ModelAndView("error");
				mav.addObject("error", imagenBase64Movil);
			}
		}catch(Exception e){
			logger.error("Error al realizar simulacion de cobro sat : ", e);
		}
		return mav;
	}
	
	private byte[] generaImagenEmail(DatosReciboVO datosReciboVO){
		Image imagenBase=null;
		Image imagenBanco=null;
		double posy=0;
		BufferedImage imgFirma=null;
		BufferedImage img = null;
		Graphics2D g2= null;
		ByteArrayOutputStream baos=null;
		byte[] imageInByte=null;
		String respuesta=null;
		try{
		    imgFirma = ImageIO.read(new ByteArrayInputStream(decodeImage(datosReciboVO.getFirmaBase64())));
		    img = new BufferedImage(560, 650, BufferedImage.TYPE_INT_RGB);
			g2=img.createGraphics();
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, 560, 650);
			imagenBase=ImageIO.read(new File(imagenBaseEmailStr));
			imagenBanco=ImageIO.read(new File(imagenBancoEmailStr));
			g2.drawImage(imagenBase, 0,0,null);
			g2.drawImage(imagenBanco, 190,85, 180, 100, null);
			g2.drawImage(getTransparentImage(imgFirma,new Color(219, 217, 214)), 185,400, 190, 130, null);
			g2.setColor(new Color(33,31,32));
			g2.setFont(new Font("Arial", Font.BOLD, 14)); 
			g2.drawString(datosReciboVO.getLineaCaptura(), 250, 216);
			g2.setFont(new Font("Arial", Font.BOLD, 12));
			posy=245;
			g2.drawString("$ "+datosReciboVO.getMonto(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getFecha(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getNumeroOperacion(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getMedioPresentacion(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getLlaveDePago(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getAutorizacion(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getTipoTarjeta(), 290, (int)posy);
			posy+=20.8;
			g2.drawString(datosReciboVO.getNumeroTarjeta(), 290, (int)posy);
			logger.info("Termino de generar la imagen");
			baos = new ByteArrayOutputStream();
			ImageIO.write( img, "jpg", baos );
			baos.flush();
		    imageInByte = baos.toByteArray();
			//baos.close();
			logger.info("Termino de obtener bytes");
			//respuesta=encodeImage(imageInByte);
			//logger.info("Termino de codificar la imagen");
		}catch(Exception e){
			logger.error("Error al generar la imagen email", e);
		}
		return imageInByte;
	  } 
	
	public String  generaImagenMovilExito(DatosReciboVO datosReciboVO){
		Image imagenBase=null;
	    Image imagenBanco=null;
	    double posy=0;
	    String respuesta=null;
	    ByteArrayOutputStream baos=null;
		byte[] imageInByte=null;
	    try{
		    BufferedImage img = new BufferedImage(480, 612, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2=(Graphics2D)img.createGraphics();
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, 480, 612);
			imagenBase=ImageIO.read(new File(imagenBaseMovilStr));
			imagenBanco=ImageIO.read(new File(imagenBancoMovilStr));
			g2.drawImage(imagenBase, 0,0,null);
			g2.drawImage(imagenBanco, 65, 100, 350, 210, null);
			//g2.drawImage(getTransparentImage(imgFirma,Color.WHITE), 185,400, 190, 130, this);
			g2.setColor(new Color(33,31,32));
			g2.setFont(new Font("Arial", Font.BOLD, 14)); 
			g2.drawString(datosReciboVO.getLineaCaptura(), 240, 365);
			posy=385;
			g2.drawString(datosReciboVO.getMonto(), 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getFecha(), 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getSecuencia(), 240, (int)posy);
			posy+=21.2;
			g2.drawString("Internet", 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getAutorizacion(), 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getProducto(), 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getNumeroTarjeta(), 240, (int)posy);
			posy+=21.2;
			g2.drawString("3D Secure", 240, (int)posy);
			logger.info("Termino de generar la imagen");
			baos = new ByteArrayOutputStream();
			ImageIO.write( img, "jpg", baos );
			baos.flush();
		    imageInByte = baos.toByteArray();
			baos.close();
			logger.info("Termino de obtener bytes");
			respuesta=encodeImage(imageInByte);
			logger.info("Termino de codificar la imagen");
	    }catch(Exception e){
	    	System.out.println("Error al generar generaImagenEmail");
	    	e.printStackTrace();
	    }
	    return respuesta;
	}
	
	public String  generaImagenMovilError(DatosReciboVO datosReciboVO){
		Image imagenBase=null;
	    Image imagenBanco=null;
	    double posy=0;
	    String respuesta=null;
	    ByteArrayOutputStream baos=null;
		byte[] imageInByte=null;
	    try{
		    BufferedImage img = new BufferedImage(480, 612, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2=(Graphics2D)img.createGraphics();
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, 480, 612);
			imagenBase=ImageIO.read(new File(imagenBaseMovilStr));
			imagenBanco=ImageIO.read(new File(imagenBancoMovilStr));
			g2.drawImage(imagenBase, 0,0,null);
			g2.drawImage(imagenBanco, 65, 100, 350, 210, null);
			//g2.drawImage(getTransparentImage(imgFirma,Color.WHITE), 185,400, 190, 130, this);
			g2.setColor(new Color(33,31,32));
			g2.setFont(new Font("Arial", Font.BOLD, 14)); 
			g2.drawString(datosReciboVO.getLineaCaptura(), 240, 365);
			posy=385;
			g2.drawString("$ 0.00", 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getFecha(), 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getSecuencia(), 240, (int)posy);
			posy+=21.2;
			g2.drawString("Internet", 240, (int)posy);
			posy+=21.2;
			g2.drawString("0000", 240, (int)posy);
			posy+=21.2;
			g2.drawString("", 240, (int)posy);
			posy+=21.2;
			g2.drawString(datosReciboVO.getNumeroTarjeta(), 240, (int)posy);
			posy+=21.2;
			g2.drawString("Declinada", 240, (int)posy);
			logger.info("Termino de generar la imagen");
			baos = new ByteArrayOutputStream();
			ImageIO.write( img, "jpg", baos );
			baos.flush();
		    imageInByte = baos.toByteArray();
			baos.close();
			logger.info("Termino de obtener bytes");
			respuesta=encodeImage(imageInByte);
			logger.info("Termino de codificar la imagen");
	    }catch(Exception e){
	    	System.out.println("Error al generar generaImagenEmail");
	    	e.printStackTrace();
	    }
	    return respuesta;
	}
	
	
	/**
     * Encodes the byte array into base64 string
     *
     * @param imageByteArray - byte array
     * @return String a {@link java.lang.String}
     */
    public static String encodeImage(byte[] imageByteArray) throws Exception {
        return new String(Base64.encodeBase64(imageByteArray),"UTF-8");
    }
     
    /**
     * Decodes the base64 string into byte array
     *
     * @param imageDataString - a {@link java.lang.String}
     * @return byte array
     */
    public static byte[] decodeImage(String imageDataString) throws Exception  {
        return Base64.decodeBase64(imageDataString.getBytes("UTF-8"));
    }
    

    public static BufferedImage getTransparentImage( BufferedImage image, Color transparent) {
        // must have a transparent image
        BufferedImage img = new BufferedImage(
            image.getWidth(),image.getHeight(),BufferedImage.TYPE_INT_ARGB);
        Graphics2D g= img.createGraphics();
        for (int x=0; x<img.getWidth(); x++) {
            for (int y=0; y<img.getHeight(); y++) {
                if (image.getRGB(x,y)!=transparent.getRGB()) {
                    img.setRGB( x,y, image.getRGB(x,y) );
                }
            }
        }
        g.dispose();
        return img;
    }

	
}