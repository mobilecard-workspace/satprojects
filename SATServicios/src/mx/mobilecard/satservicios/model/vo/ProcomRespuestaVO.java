package mx.mobilecard.satservicios.model.vo;

public class ProcomRespuestaVO {
	//Atributos obligatorios
	private String emResponse;
	private String emTotal;
	private String emOrderId;
	private String emMerchant;
	private String emStore;
	private String emTerm;
	private String emRefNum;
	private String emAuth;
	private String emDigest;
	private String firmaBase64;
	private String email;
	private String lineaCapturaSat;
	
	
	
	public String getFirmaBase64() {
		return firmaBase64;
	}
	public void setFirmaBase64(String firmaBase64) {
		this.firmaBase64 = firmaBase64;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLineaCapturaSat() {
		return lineaCapturaSat;
	}
	public void setLineaCapturaSat(String lineaCapturaSat) {
		this.lineaCapturaSat = lineaCapturaSat;
	}
	public String getEmResponse() {
		return emResponse;
	}
	public void setEmResponse(String emResponse) {
		this.emResponse = emResponse;
	}
	public String getEmTotal() {
		return emTotal;
	}
	public void setEmTotal(String emTotal) {
		this.emTotal = emTotal;
	}
	public String getEmOrderId() {
		return emOrderId;
	}
	public void setEmOrderId(String emOrderId) {
		this.emOrderId = emOrderId;
	}
	public String getEmMerchant() {
		return emMerchant;
	}
	public void setEmMerchant(String emMerchant) {
		this.emMerchant = emMerchant;
	}
	public String getEmStore() {
		return emStore;
	}
	public void setEmStore(String emStore) {
		this.emStore = emStore;
	}
	public String getEmTerm() {
		return emTerm;
	}
	public void setEmTerm(String emTerm) {
		this.emTerm = emTerm;
	}
	public String getEmRefNum() {
		return emRefNum;
	}
	public void setEmRefNum(String emRefNum) {
		this.emRefNum = emRefNum;
	}
	public String getEmAuth() {
		return emAuth;
	}
	public void setEmAuth(String emAuth) {
		this.emAuth = emAuth;
	}
	public String getEmDigest() {
		return emDigest;
	}
	public void setEmDigest(String emDigest) {
		this.emDigest = emDigest;
	}		
}

