package mx.mobilecard.satservicios.model.vo;

public class RequestPagoMovilesVO {
	private String email;
	private String firmaBase64;
	private String lineaDeCapturaSat;
	private String monto;
	
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirmaBase64() {
		return firmaBase64;
	}
	public void setFirmaBase64(String firmaBase64) {
		this.firmaBase64 = firmaBase64;
	}
	public String getLineaDeCapturaSat() {
		return lineaDeCapturaSat;
	}
	public void setLineaDeCapturaSat(String lineaDeCapturaSat) {
		this.lineaDeCapturaSat = lineaDeCapturaSat;
	}
	
}
