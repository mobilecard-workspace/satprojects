package mx.mobilecard.satservicios.model.vo;

public class DatosReciboVO {
	private String lineaCaptura;
	private String monto;
	private String fecha;
	private String numeroOperacion;
	private String medioPresentacion;
	private String llaveDePago;
	private String autorizacion;
	private String tipoTarjeta;
	private String numeroTarjeta;
	private String firmaBase64;
	private String secuencia;
	private String producto;
	
	
	
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	public String getLineaCaptura() {
		return lineaCaptura;
	}
	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNumeroOperacion() {
		return numeroOperacion;
	}
	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}
	public String getMedioPresentacion() {
		return medioPresentacion;
	}
	public void setMedioPresentacion(String medioPresentacion) {
		this.medioPresentacion = medioPresentacion;
	}
	public String getLlaveDePago() {
		return llaveDePago;
	}
	public void setLlaveDePago(String llaveDePago) {
		this.llaveDePago = llaveDePago;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getFirmaBase64() {
		return firmaBase64;
	}
	public void setFirmaBase64(String firmaBase64) {
		this.firmaBase64 = firmaBase64;
	}
	
}
