package mx.mobilecard.satservicios.model.vo;

public class DatosCorreoVO {
	private String descServicio;
	private String imgServicio;
	private String email;
	private String noAutorizacion;
	private Double monto;
	private Double comision;
	private String referenciaBanco;
	private String referenciaSat;
	private String fecha;
	private int idBitacora;
	private String ticket;
	private byte[] imagenPago;
	
	

	public byte[] getImagenPago() {
		return imagenPago;
	}
	public void setImagenPago(byte[] imagenPago) {
		this.imagenPago = imagenPago;
	}
	public String getReferenciaSat() {
		return referenciaSat;
	}
	public void setReferenciaSat(String referenciaSat) {
		this.referenciaSat = referenciaSat;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getImgServicio() {
		return imgServicio;
	}
	public void setImgServicio(String imgServicio) {
		this.imgServicio = imgServicio;
	}
	public String getDescServicio() {
		return descServicio;
	}
	public void setDescServicio(String descServicio) {
		this.descServicio = descServicio;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Double getComision() {
		return comision;
	}
	public void setComision(Double comision) {
		this.comision = comision;
	}
	public String getReferenciaBanco() {
		return referenciaBanco;
	}
	public void setReferenciaBanco(String referenciaBanco) {
		this.referenciaBanco = referenciaBanco;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getNoAutorizacion() {
		return noAutorizacion;
	}
	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}
	
	
}
