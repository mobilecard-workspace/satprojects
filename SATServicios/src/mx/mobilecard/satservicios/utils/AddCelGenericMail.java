package mx.mobilecard.satservicios.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import mx.mobilecard.satservicios.model.vo.CorreoVO;
import mx.mobilecard.satservicios.model.vo.DatosCorreoVO;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
//	private static final String urlString = "http://50.57.192.213:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	private static final String HTML_DOBY = "<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta name=\"viewport\" content=\"width=device-width\" /> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> <title>Mobilecard</title> <style type=\"text/css\">  body { background: none repeat scroll 0 0 #FFFFFF; font-family: 'Open Sans', sans-serif;  } table.info_description{ height: auto; width: 620px; position: relative; } table.info_description td{ height: auto; color: #DF1D44; font-size: 30px; line-height: 32px; text-align: center; display: inline-block; vertical-align: top; *display: inline; *zoom:1; } section.helpers{ background: none repeat scroll 0 0 #FFFFFF; width: 940px; height: auto; position: relative; padding: 26px 0 0; margin: 0 auto; vertical-align: center; color: #000000; font-weight: bold; text-transform:uppercase; font-size: 12px; line-height: 14px; text-align: center; } section.helpers div.extra_text_helpers{ margin: 44px auto 20px;  } section.helpers div.footer_text_helpers{ margin: 0 0 0 0;  } </style> </head> <body> <table class=\"info_description\"> <tr> <td><img src=\"cid:identifierCID00\" alt=\"Embedded Image\"/></td> </tr> </table> <div style=\" background: none repeat scroll 0 0 #FFFFFF; width: 940px; height: auto; position: relative; padding: 26px 0 0; margin: 0 auto; vertical-align: center; color: #000000; font-weight: bold; text-transform:uppercase; font-size: 12px; line-height: 14px; text-align: center; \"> <div class=\"extra_text_helpers\"> Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo. </div> <div class=\"footer_text_helpers\"> Gracias por usar <div><img src=\"cid:identifierCID01\" alt=\"\"/> </div> </div> </div> </body> </html>";
			

	public static CorreoVO generatedMail(DatosCorreoVO datosCorreoVO){
		logger.info("Genera objeto para envio de mail: " + datosCorreoVO.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
	//		String[] attachments = {src_file};
			/*String body = HTML_DOBY.toString();
			body = body.replaceAll("<#PRODUCTO#>", "Pago Megacable");
			body = body.replaceAll("<#MONTO#>", datosPagoVO.getMonto());
			body = body.replaceAll("<#MONEDA#>", "MXN");
			body = body.replaceAll("<#FECHA#>", (datosPagoVO.getFecha()!= null ? datosPagoVO.getFecha().substring(0, 19) : ""));
			body = body.replaceAll("<#AUTBAN#>", datosPagoVO.getNoAutorizacion()!= null ? datosPagoVO.getNoAutorizacion() : "");
			body = body.replaceAll("<#REFE#>", String.valueOf(datosPagoVO.getIdBitacora()));
			body = body.replaceAll("<#VOUCHER#>", String.valueOf(datosPagoVO.getVoucher()));*/
			String body = HTML_DOBY.toString();
			//body=body.replaceAll("<#IMAGEN_BASE64#>", datosCorreoVO.getImagenPago());
			String from = "no-reply@addcel.com";
			String subject = "Acuse pago SAT - Referencia: " + datosCorreoVO.getReferenciaSat();
			String[] to = {datosCorreoVO.getEmail()};
			Object[] cid = {datosCorreoVO.getImagenPago(),"/usr/java/resources/images/SAT/logoMC.png"};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}

}
