package mx.mobilecard.satservicios.services;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


public class UtilsService {
	private static final Logger logger = Logger.getLogger(UtilsService.class);
	private static ObjectMapper mapperJk;	
	
	public UtilsService(){
		mapperJk=new ObjectMapper();
	}
	
	public  static <T> String objectToJson(T object){
		String json=null;
		try {
			mapperJk.getSerializationConfig().setSerializationInclusion(Inclusion.NON_NULL);
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			logger.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			logger.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			logger.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public static <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			logger.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			logger.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			logger.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
}
