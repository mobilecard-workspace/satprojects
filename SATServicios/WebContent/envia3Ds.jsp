<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">-->
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
<%--
#java/jsp/html
################################################################################
# Nombre del Programa :prosa_comercio_validaciones.jsp                         #
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :N/A                                   Fecha: N/A        #
# Descripcion General :Pagina para comercio electronico                        # 
# Programa Dependiente:N/A                                                     #
# Programa Subsecuente:N/A                                                     #
# Cond. de ejecucion  :N/A                                                     #
# Dias de ejecucion   :N/A                                      Horario:N/A    #
#                              MODIFICACIONES                                  #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10                             Fecha:14/10/10 #
# Modificacion        :Nivelacion de Procom                                    #
# Marca de cambio     :C-04-2761-10 Acriter NAC                                #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10 Fase2                       Fecha:20/01/11 #
# Modificacion        :Nivelacion de Procom Fase2                              #
# Marca de cambio     :Acriter NAC C-04-2761-10 Fase2                          #
#------------------------------------------------------------------------------#
# Numero de Parametros:N/A                                                     #
# Parametros Entrada  :N/A                                      Formato:N/A    #
# Parametros Salida   :N/A                                      Formato:N/A    #
################################################################################
--%>
<head>
    <title>Purchase Verification</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="HandheldFriendly" content="true"/> 
    	<style>
	    * {
  
		  /*with these codes padding and border does not increase it's width.Gives intuitive style.*/
		  
		  -webkit-box-sizing: border-box;   
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  color:#33313;
		  font-family: "Arial", Helvetica, sans-serif;
		}

		body {
		   margin:0;
		   padding:0;		  
		  }
		div#envelope{
			width:80%;
			margin: 10px 10% 10px 10%;
			background-color:#f2f4fb;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
			} 
			
		form{
			width:70%;
			margin:0 15%;
		}  

		form header {
		  text-align:center;
		  font-family: 'Roboto Slab', serif;
		  }
		 

		/* Makes responsive fields.Sets size and field alignment.*/
		input[type=text],input[type=password]{
			margin-bottom: 20px;
			margin-top: 10px;
			width:100%;
			padding: 15px;
			border-radius:5px;
			border:1px solid #dbd9d6;
			font-size: 110%;
		}
		input[type=submit]
		{
			margin-bottom: 20px;
			width:100%;
			color:#000000;
			padding: 15px;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
		}
		input[type=button]
		{
			margin-bottom: 20px;
			width:100%;
			padding: 15px;
			color:#000000;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
		}
		textarea{
			width:100%;
			padding: 15px;
			margin-top: 10px;
			border:1px solid #7ac9b7;
			border-radius:5px; 
			margin-bottom: 20px;
			resize:none;
		  }
		 
		input[type=text]:focus, textarea:focus, select:focus {
		  border-color: #333132;
		}
		
		.styled-select {
			width:100%;
			overflow: hidden;
			background: #FFFFFF;
			border-radius:5px;
			border:1px solid #dbd9d6;
		 }
		 
		.styled-select select {
			font-size: 110%;
			width: 100%;
			border: 0 !important;
			padding: 15px 0px 15px 0px;
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			text-indent:  0.01px;
			text-overflow: '';
			
		}
	</style>
</head>
<%@page import="java.util.Enumeration"%>
<%//@page import="com.acriter.abi.procom.utils.StringHelper"%>
<%//@page import="com.acriter.abi.procom.model.constants.RequestParam"%>
<body>

<% 
String host = request.getParameter("host");
String sessionid = request.getParameter("sessionid");
Enumeration en = request.getParameterNames();


if (host != null && !host.equals("null") && !host.equals("")  && sessionid != null && !sessionid.equals("null") && !sessionid.equals("")) { %>
<link rel=stylesheet href="http://<%= host %>/clear.png?session=<%= sessionid %>">
<object type="application/x-shockwave-flash" data="https://<%= host %>/fp.swf" width="1" height="1" id="thm_fp"><param name="movie" value="https://<%= host %>/fp.swf"/><param name="FlashVars" value="session=<%= sessionid %>" /></object>
<script src="https://<%= host %>/check.js?session=<%= sessionid %>" type="text/javascript"></script>
<% } %>

<div id="envelope">
	<%-- <p style="text-align: center;">
			<img src="https://www.mobilecard.mx:8443/ADOServicios/resources/logo-ado.jpg" width="100px" height="30px" align="center"/>
		</p>--%>
		<p style="text-align: center;">Portal 3D Secure para pago M&oacute;vil SAT</p>
		<p style="text-align: center;">Por favor proporcione la siguiente informaci&oacute;n:</p>
		<br/>
		<!-- Invalidando Session -->
		<%session.invalidate();%>
		<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 --> 
		<FORM METHOD="POST" name ="forma3ds" id="forma3ds" AUTOCOMPLETE="OFF" ACTION="./realizaPago3Ds">
			<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
				<p> Informaci&oacute;n de la tarjeta de Cr&eacute;dito:</p>
				<input type="hidden" name="data_sent" value="1">
				<!-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- -->
				<input type="hidden" name="returnContext" value="<%=request.getContextPath()%>"/>
				<input type="hidden" name="urlMerchant" value="<%=request.getServletPath()%>"/>
				<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
				<input type="hidden" name="urlpost" value="/urlpost.jsp"/>
				<input type="hidden" name="urlerror" value="/urlpost.jsp"/>
				<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
				<input type="hidden" name="acquirer" value="83">
				<input type="hidden" name="source" value="100">


				
				<%	
					String name = null;
					String value = null;

					while(en.hasMoreElements()){
					  name = (String)en.nextElement();
					  value = request.getParameter(name);
					  %>
					  <input type="hidden" name="<%=name%>" value="<%=value%>">
					  <%				
					}%>
				<label for="cc_name">Nombre:</label>
				<input type="text" name="cc_name" size="40,1" maxlength="40" value="" required="true" />
				
				<label for="cc_number">N&uacute;mero de Tarjeta:</label>
				<input type="text" name="cc_number" size="40,1"	maxlength="19" value="" required="true" />

				<label for="cc_type">Tipo:</label><br/>
				<div class="styled-select">
					<select name="cc_type" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
						<option value="Visa">VISA</option>
						<option value="Mastercard">MasterCard</option>
					</select>
				</div>
				<div style="width:100%;padding-top:10px">
					<label for ="_cc_expmonth">Fecha de Vencimiento (mes-a&ntilde;o):</label><br/>
					<div class="styled-select" style="float:left;width:50%;">
						<select  name="_cc_expmonth" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
							<option value="01">1</option>
							<option value="02">2</option>
							<option value="03">3</option>
							<option value="04">4</option>
							<option value="05">5</option>
							<option value="06">6</option>
							<option value="07">7</option>
							<option value="08">8</option>
							<option value="09">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select> 
					</div>
					<div  class="styled-select" style="float:right;width:50%;">
						<select  name="_cc_expyear" class="selmenu">
								<%
									java.util.Calendar C = java.util.Calendar.getInstance();
									int anio = C.get(java.util.Calendar.YEAR);
									for (int i = 0; i < 15; i++) {
										out.println("<option value=\""+anio+"\">" + anio+ "</option>");
										anio++;
									}
								%>

						</select>
					</div>
				</div>
				<div style="width:100%;padding-top:10px">
					<label for ="cc_cvv2">C&oacute;digo de seguridad(CVV2/CVC2):</label>
					<input type="password" name="cc_cvv2" size="3,1" maxlength="3" value="" required="true" class="txtinput"/>
				</div>
				<p>Favor de verificar los datos y seleccionar el bot&oacute;n Pagar para efecturar el cargo a su tarjeta</p>
				<div style="width:100%;padding-top:10px">
					<input type="submit" value="Pagar" />
				</div>
				<div style="width:100%;padding-top:10px">
					<input type="button" value="Cancelar" />
				</div>
			</div>
		</form>
	</div>
</body>
</html>
