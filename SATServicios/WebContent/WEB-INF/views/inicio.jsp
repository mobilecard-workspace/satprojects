<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.HashMap"%>
<%@page import="mx.mobilecard.crypto.SHA" %>
<%@page import="mx.mobilecard.satservicios.model.vo.RequestPagoMovilesVO" %>
<%@page import="mx.mobilecard.satservicios.services.UtilsService" %>
<%@page import="com.addcel.utils.AddcelCrypto"%>

<!DOCTYPE html>
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Envio</title>
        <script language="JavaScript">
        function sendform3DSecure(){
            if(self.name.length === 0) {
                self.name = "gotoProsa";
            }
            document.form1.return_target.value=self.name.toString();
            document.form1.submit();
        }
        </script>        
    </head>

    <body onload="sendform3DSecure()">   
       <FORM METHOD="post" NAME ="form1" id="form3DSecureEnvia" action="./envia3Ds.jsp">
            <input type="hidden" name="currency" value="484">
            <input type="hidden" name="address" value="PROSA">
            <input type="hidden" name="order_id" value="<%=((int)(Math.random()*10000))%>">
            <input type="hidden" name="merchant" value="9635822">
            <input type="hidden" name="store" value="1234">
            <input type="hidden" name="term" value="001">
            <input type="hidden" name="digest" value="12345678901234567890">
            <input type="hidden" name="return_target" value="">         
            <input type="hidden" name="urlBack" value="/realizaPago">
            <input type="hidden" name="total" value="${monto}">
            <input type="hidden" name="param1" value="${email}">
            <input type="hidden" name="param2" value="${lineaDeCapturaSat}">
            <input type="hidden" name="param3" value="${firmaBase64}">
        </FORM>
    </body>
</html>
