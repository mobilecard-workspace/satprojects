package addcel.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

public final class ImageUtils {

    private static final String RESOURCE_FOLDER = "drawable";

	
	private ImageUtils() {
		
	}
	
	public static Drawable getDrawableCompat(Context context, int drawable) {
		
		if(Build.VERSION.SDK_INT >= 21){
		    return context.getResources().getDrawable(drawable, context.getTheme());
		} else { 
		    return context.getResources().getDrawable(drawable);
		} 
	}

	public static void setDrawableCompat(ImageView imageView, Drawable drawable) {
		if (Build.VERSION.SDK_INT >= 16) {
			imageView.setBackground(drawable);
		} else {
			imageView.setBackgroundDrawable(drawable);
		}
	}

    public static Drawable getDrawableFromIdentifier(Context context, String identifier) {

        Drawable drawable = null;

        int id = context.getResources().getIdentifier(
                identifier, RESOURCE_FOLDER, context.getPackageName());

        if (id != 0) {
            drawable = ImageUtils.getDrawableCompat(context, id);
        }

        return drawable;
    }
	
	
	
}
