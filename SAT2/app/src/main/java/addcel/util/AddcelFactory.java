package addcel.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.addcel.android.crypto.AddcelCrypto;

import okio.ByteString;

/**
 * Created by carlosgs on 03/08/15.
 */
public final class AddcelFactory {

    private static Gson gson;
    private static ProgressDialog pDialog;

    private AddcelFactory() {

    }

    public static synchronized Gson getGson() {
        if (gson == null) {
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            gson = builder.create();
        }

        return gson;
    }

    public static synchronized ProgressDialog getpDialog(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(true);

        return pDialog;
    }

    public static synchronized byte[] toByteArray(Object o) {
        String s = AddcelFactory.getGson().toJson(o);
        Log.d("toByteArray", s);
        s = "json=" + AddcelCrypto.encryptSensitive(s);
        return ByteString.encodeUtf8(s).toByteArray();

    }


}
