package addcel.util;

import android.content.Context;
import android.text.format.DateFormat;

import com.squareup.phrase.Phrase;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import okio.ByteString;

/**
 * Created by carlosgs on 27/08/15.
 */
public final class AddcelTextUtils {

    private static final Locale MX = new Locale("es", "MX");
    private static NumberFormat FORMAT;
    private static CharSequence SEQ;

    private AddcelTextUtils() {

    }

    private static synchronized NumberFormat getFORMAT() {
        if (FORMAT == null) {
            FORMAT = NumberFormat.getCurrencyInstance(MX);
            FORMAT.setMaximumFractionDigits(2);
        }

        return FORMAT;
    }

    public static synchronized CharSequence getCurrencyString(double amount) {
        return getFORMAT().format(amount);
    }

    public static synchronized CharSequence getCurrencyString(Context context, int stringRes, double amount) {
        return Phrase.from(context, stringRes).put("monto", getFORMAT().format(amount)).format();
    }

    public static synchronized CharSequence getLabelText(Context context, int stringRes, String key, CharSequence value) {

        SEQ = Phrase.from(context, stringRes).put(key, value).format();

        return SEQ;
    }

    public static synchronized CharSequence getDateString(long timeInMillis) {

        return DateFormat.format("dd/MM/yyyy", timeInMillis);

    }

}
