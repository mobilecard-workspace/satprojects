package addcel.util;

import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.crypto.Crypto;
import org.addcel.android.crypto.CryptoUtils;

import java.lang.reflect.Type;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AddcelClient {
	
	private OkHttpClient client;
	private static final boolean DEBUG = true;
	
	private static AddcelClient instance;
	private static final String TAG = "AddcelClient";
	
	private AddcelClient() {
		
		client = new OkHttpClient();
		client.setReadTimeout(35, TimeUnit.SECONDS);
		client.setConnectTimeout(10, TimeUnit.SECONDS);
		
		try {
				    // Create a trust manager that does not validate certificate chains
			final TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					  @Override
					  public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
							  String authType) throws CertificateException {
					  }
					
					  @Override
					  public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
							  String authType) throws CertificateException {
					  }
					
					  @Override
					  public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					    return null;
					  }
				}
			};
		
			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
			
			client.setSslSocketFactory(sslSocketFactory);
			client.setHostnameVerifier(new HostnameVerifier() {
		
				@Override
				public boolean verify(String hostname, SSLSession session) {
					// TODO Auto-generated method stub
					return true;
				}
			});
				
		} catch (Exception e) {
			Log.e(TAG, TrustManager.class.getSimpleName(), e);
		}
	}
	
	public static synchronized AddcelClient get() {
		if (instance == null)
			instance = new AddcelClient();
		
		return instance;
	}
	
	public String get(String url, Cifrados cifrado) {
		return post(url, "", cifrado);
	}
	
	public <T> T get(String url, Cifrados cifrados, Class<T> type) {
		return buildResponse(post(url, "", cifrados), type);
	}

	public <T> T get(String url, Cifrados cifrados, Type type) {
		return buildResponse(post(url, "", cifrados), type);
	}
	
	//MOBILECARD
	public String postMobilecard(String url, String param, String key) {
		return post(url, param, Cifrados.MOBILECARD, key);
	}
	
	public <T> T postMobilecard(String url, String param, String key, Class<T> type) {
		return buildResponse(postMobilecard(url, param, key), type);
	}
	
	public String postMobilecard(String url, Object param, String key) {
		String paramOne = AddcelFactory.getGson().toJson(param);
		return postMobilecard(url, paramOne, key);
	}
	
	public <T> T postMobilecard(String url, Object param, String key, Class<T> type) {
		return buildResponse(postMobilecard(url, param, key), type);
	}
	
	//SENSITIVE & HARD STRING
	public String post(String url, String param, Cifrados cifrado) {
		if (!cifrado.equals(Cifrados.MOBILECARD))
			return post(url, param, cifrado, "");
		else 
			return "";
	}
	
	public <T> T post(String url, String param, Cifrados cifrado, Class<T> type) {
		return buildResponse(post(url, param, cifrado), type);
	}
	
	public String post(String url, Object param, Cifrados cifrado) {
		String paramOne = AddcelFactory.getGson().toJson(param);
		return post(url, paramOne, cifrado);
		
	}
	
	public <T> T post(String url, Object param, Cifrados cifrado, Class<T> type) {
		String paramOne = AddcelFactory.getGson().toJson(param);
		return buildResponse(post(url, paramOne, cifrado), type);
		
	}
	
	//RECIBEN PARAMETRO STRING
	
	public String post(String url, String param, Cifrados cifrado, String key) {
		
		Request request;
		RequestBody body = null;
	
		if (param != null) {
			if (DEBUG)
				Log.d(TAG, param); //Petición limpia
			
			switch (cifrado) {
			case SENSITIVE:
			case MIXED:
				param = AddcelCrypto.encryptSensitive(param).trim();
				break;

			case HARD:
				param = AddcelCrypto.encryptHard(param).trim();
				break;
			
			case MOBILECARD:
				param = Crypto.aesEncrypt(CryptoUtils.parsePass(key), param);
				param = CryptoUtils.mergeStr(param, key).trim();
				break;
				
			default:
				break;
			}
			
			body = new FormEncodingBuilder().add("json", param).build();
		} else {
			body = RequestBody.create(MediaType.parse("text/plain"), "");
		}
		if (DEBUG)
			Log.d(TAG, url + (TextUtils.isEmpty(param) ? "": "?json="+param));
			
		request = new Request.Builder().url(url).post(body).build();
		
		try {
			Response response = client.newCall(request).execute();
			
			if (response.code() == 200) {
				String data = response.body().string();
				
				if (DEBUG)
					Log.d(TAG, "Respuesta cifrada: " + data);
				
				try {
					switch (cifrado) {
					case SENSITIVE:
						data = AddcelCrypto.decryptSensitive(data).trim();
						break;
					case HARD:
					case MIXED:
						data = AddcelCrypto.decryptHard(data).trim();
						break;
					case MOBILECARD:
						data = Crypto.aesDecrypt(CryptoUtils.parsePass(key), data).trim();
						break;
					default:
						break;
					}
				} catch (Exception e) {
					Log.e(TAG, "Error al descifrar respuesta", e);
//					data = null;
				}
				
				if (DEBUG)
					Log.d(TAG, "Respuesta limpia: " + (data == null ? "404" : data));
				
				return data;
			} else {
				String msg = TextUtils.isEmpty(response.message()) ? "NULL": response.message();
				Log.e(TAG, "Error: " + response.code() + " Msg: " + msg);
				return null;
			}
		} catch (Exception e) {
			Log.e(TAG, "Method: post()", e);
			
			return null;
		}
		
	}
	
	public <T> T post(String url, String param, Cifrados cifrado, String key, Class<T> type) {
		return buildResponse(post(url, param, cifrado, key), type);
	}
		
	public <T>T buildResponse(String response, Class<T> type) {
		try {
			return AddcelFactory.getGson().fromJson(response, type);
		} catch (Exception e) {
			Log.e(TAG, "buildResponse()", e);
			return null;	
		}
	}

	public <T>T buildResponse(String response, Type type) {
		try {
			return AddcelFactory.getGson().fromJson(response, type);
		} catch (Exception e) {
			Log.e(TAG, "buildResponse()", e);
			return null;
		}
	}
}
