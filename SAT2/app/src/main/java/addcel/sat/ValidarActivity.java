package addcel.sat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import addcel.sat.callback.ValidarCallback;
import addcel.sat.presenter.ValidarPresenter;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.ConsultaResponse;
import addcel.sat.to.response.DefaultResponse;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ValidarActivity extends Activity implements ValidarCallback {

    @Bind(R.id.text_lc_captura)
    EditText capturaText;
    @Bind(R.id.text_lc_total)
    EditText totalText;

    private ValidarPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validar);
        ButterKnife.bind(this);
        presenter = new ValidarPresenter(this, this);
    }

    private boolean validar() {
        CharSequence lc = capturaText.getText();
        CharSequence monto = totalText.getText();

        if (TextUtils.isEmpty(lc)) {
            capturaText.requestFocus();
            capturaText.setError(getText(R.string.error_lc_captura_vacia));
            return false;
        }

        if (TextUtils.isEmpty(monto) || !TextUtils.isDigitsOnly(monto)) {
            totalText.requestFocus();
            totalText.setError(getText(R.string.error_lc_monto));
            return false;
        }

        return true;
    }

    @OnClick(R.id.b_lc_enviar)
    void enviar() {

        if (validar()) {
            presenter.validaLineaAsync(
                    capturaText.getText().toString().trim(),
                    Double.parseDouble(totalText.getText().toString().trim()));
        }
    }

    @Override
    public void onLineaValida(PagoData response) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("consulta", Parcels.wrap(PagoData.class, response));
        Intent intent = new Intent(this, DatosPagoActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onError(DefaultResponse error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onLineaConsultada(PagoData response) {

    }
}
