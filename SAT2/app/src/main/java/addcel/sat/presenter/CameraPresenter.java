package addcel.sat.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import org.w3c.dom.Text;

import addcel.sat.R;
import addcel.sat.callback.CameraCallback;
import addcel.sat.callback.ValidarCallback;
import addcel.sat.to.response.DefaultResponse;

/**
 * Created by carlosgs on 05/09/15.
 */
public class CameraPresenter extends ValidarPresenter {

    private Context mContext;
    private CameraCallback mCallback;
    private ProgressDialog pDialog;
    private BarcodeFormat barcodeFormat;

    private static final String TAG = "CameraPresenter";

    public CameraPresenter(Context context, CameraCallback callback) {
        super(context, callback);
        mContext = context;
        mCallback = callback;
    }

    public CameraPresenter(Context context, CameraCallback callback, BarcodeFormat format) {
        super(context, callback);
        mContext = context;
        mCallback = callback;
        barcodeFormat = format;
    }

    public void validarCodigo(Result rawResult) {

        if (rawResult == null) {
               mCallback.onError(
                       new DefaultResponse(-1000, getRes().getString(R.string.error_camera_null)));
        } else {

            if (TextUtils.isEmpty(rawResult.getText()))
                mCallback.onError(
                        new DefaultResponse(
                                -1000, getRes().getString(R.string.error_camera_null)));
            else {
                if (barcodeFormat.equals(BarcodeFormat.QR_CODE) &&
                        rawResult.getBarcodeFormat().equals(BarcodeFormat.QR_CODE)) {
                    Log.d(TAG, "Contenido barcode: " + rawResult.getText());
                    mCallback.onValidCode(rawResult.getText());
                } else  if (!barcodeFormat.equals(BarcodeFormat.QR_CODE) &&
                        !rawResult.getBarcodeFormat().equals(BarcodeFormat.QR_CODE)) {
                    Log.d(TAG, "Contenido barcode: " + rawResult.getText());
                    mCallback.onValidCode(rawResult.getText());
                } else {
                    mCallback.onError(new DefaultResponse(
                            -1000, getRes().getString(R.string.error_camera_format)));
                }
            }
        }
    }


}
