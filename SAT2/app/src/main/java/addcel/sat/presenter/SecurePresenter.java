package addcel.sat.presenter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Calendar;

import addcel.sat.MenuActivity;
import addcel.sat.R;
import addcel.sat.SecureActivity;
import addcel.sat.Url;
import addcel.sat.callback.SecureCallback;
import addcel.sat.to.PagoData;
import addcel.util.AddcelFactory;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by carlosgs on 07/09/15.
 */

@SuppressLint("SetJavaScriptEnabled")
public class SecurePresenter extends WebViewClient implements SecureCallback{

    private Context mContext;
    private PagoData mData;
    private ProgressDialog pDialog;
    private boolean finished;
    private AlertDialog finishDialog;

    private static final String FORM_INDICATOR = "envia3Ds.jsp";
    private static final String FINISH_INDICATOR = "realizaPago3Ds";
    private static final String PROCESS_HTML = "javascript:window.HTMLOUT.processHTML("
            + "'<html>'"
            + 	"+document.getElementsByTagName('html')[0].innerHTML"
            + "+'</html>'"
            + ");";

    @Bind(R.id.browser)
    WebView browser;

    private static final String TAG = "SecurePresenter";

    @SuppressLint("AddJavascriptInterface")
    public SecurePresenter(Context context, PagoData data) {
        mContext = context;
        mData = data;
        ButterKnife.bind(this, (Activity) mContext);
        pDialog = AddcelFactory.getpDialog(mContext);
        pDialog.setMessage(mContext.getText(R.string.progress_pago));
        finished = false;


        setUpWebViewDefaults(browser);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1)
            browser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        browser.addJavascriptInterface(this, "HTMLOUT");

        browser.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                ((SecureActivity) mContext).setProgress(newProgress);
            }
        });

        browser.setWebViewClient(this);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setUpWebViewDefaults(WebView webView) {
        WebSettings settings = webView.getSettings();

        // Enable Javascript
        settings.setJavaScriptEnabled(true);

        // Use WideViewport and Zoom out if there is no viewport defined
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        // Enable pinch to zoom without the zoom buttons
        settings.setBuiltInZoomControls(true);

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            // Hide the zoom controls for HONEYCOMB+
            settings.setDisplayZoomControls(false);
        }

        // Enable remote debugging via chrome://inspect
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }


    public void initTransaction() {
        browser.postUrl(Url.PAGO, AddcelFactory.toByteArray(mData));
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public AlertDialog getFinishDialog() {

        if (finishDialog == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(R.string.title_secure_pago);
            builder.setMessage(R.string.msg_secure_pago);
            builder.setNegativeButton(R.string.txt_pago_reintentar, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    ((SecureActivity) mContext).superOnBackPressed();
                }
            });
            builder.setNeutralButton(R.string.txt_pago_salir, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(mContext, MenuActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    mContext.startActivity(intent);
                }
            });
            builder.setPositiveButton(R.string.txt_pago_continuar, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });

            finishDialog = builder.create();

        }

        return finishDialog;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Log.d(TAG, url);
        pDialog.show();
    }

    @Override
    public void onPageFinished(WebView view, String url) {

        pDialog.dismiss();

        view.loadUrl(PROCESS_HTML);

        if (TextUtils.indexOf(url, FORM_INDICATOR) >= 0)
            view.loadUrl(
                    buildInjectableString(
                            mData.getNombre(), mData.getCard().cardNumber,
                            mData.getCard().getCardType().name, mData.getCard().expiryMonth,
                            mData.getCard().expiryYear));

        if (TextUtils.indexOf(url, FINISH_INDICATOR) >= 0)
            finished = true;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode,
                                String description, String failingUrl) {
        // TODO Auto-generated method stub
        Log.e(TAG, "Url fallida: " + failingUrl);
        Log.e(TAG, "Error: " + description);
        Toast.makeText(mContext, "Error al cargar: " + failingUrl,
                Toast.LENGTH_LONG).show();
    }


    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        Log.e(TAG, error.toString());
        handler.proceed();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    public void onBackPressed() {
        if (isFinished()) {
            Intent intent = new Intent(mContext, MenuActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            mContext.startActivity(intent);
        } else {
            getFinishDialog().show();
        }
    }

    private String buildInjectableString(String clientName, String card, String type,
                                           int mes, int anio) {
        int indexMes = mes - 1;
        int indexAnio = anio - Calendar.getInstance().get(Calendar.YEAR);

        String fun = "";

        StringBuilder builder = new StringBuilder(
                "javascript:document.getElementsByName('cc_name')[0].value='")
                .append(clientName)
                .append("';")
                .append("document.getElementsByName('cc_number')[0].value='")
                .append(card)
                .append("';")
                .append("document.getElementsByName('cc_type')[0].value='")
                .append(type)
                .append("';")
                .append("document.getElementsByName('cc_cvv2')[0].value='")
                .append("")
                .append("';")
                .append("document.getElementsByName('_cc_expmonth')[0].selectedIndex=")
                .append(indexMes)
                .append(";")
                .append("document.getElementsByName('_cc_expyear')[0].selectedIndex=")
                .append(indexAnio).append(";");

        fun = builder.toString().trim();
        Log.d(TAG, fun);

        return fun;
    }

    @JavascriptInterface
    @Override
    public void processHTML(String html) {
        Log.d(TAG, html);
    }
}
