package addcel.sat.presenter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;

import addcel.sat.R;
import addcel.sat.SecureActivity;
import addcel.sat.api.SatClient;
import addcel.sat.api.SatClientFactory;
import addcel.sat.callback.PagoCallback;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.DefaultResponse;
import addcel.sat.to.response.TokenResponse;
import addcel.util.AddcelFactory;
import addcel.util.DeviceUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import io.card.payment.CreditCard;

/**
 * Created by carlosgs on 05/09/15.
 */
public class PagoPresenter {

    private Context mContext;
    private PagoCallback mCallback;
    private ProgressDialog pDialog;
    private Resources res;
    private PagoData mData;
    private CreditCard tarjeta;
    private AlertDialog signatureDialog;
    private SatClient client = SatClientFactory.get(true);

    public static final int SCAN_REQUEST = 1;

    public PagoPresenter(Context context, PagoCallback callback, PagoData response) {
        mContext = context;
        mCallback = callback;
        mData = response;
        pDialog = AddcelFactory.getpDialog(mContext);
        res = mContext.getResources();
    }

    public PagoData getmData() {
        return mData;
    }

    public void setmData(PagoData mData) {
        this.mData = mData;
    }

    public CreditCard getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(CreditCard tarjeta) {
        this.tarjeta = tarjeta;
    }

    public AlertDialog getSignatureDialog() {

        if (signatureDialog == null) {

            View v = LayoutInflater.from(mContext).inflate(R.layout.view_firma, null);
            final ViewHolder holder = new ViewHolder(v);

            AlertDialog.Builder b = new AlertDialog.Builder(mContext);


            b.setTitle(res.getText(R.string.title_pago_firma));
            b.setView(v);
            b.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    holder.sView.clear();
                }
            });
            b.setNegativeButton("Limpiar", null);
            b.setPositiveButton("Autorizar", null);

            signatureDialog = b.create();

            signatureDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button negativeButton = signatureDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    negativeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!holder.sView.isEmpty())
                                holder.sView.clear();
                        }
                    });

                    Button positiveButton = signatureDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (holder.sView.isEmpty()) {
                                Toast.makeText(mContext, R.string.error_pago_firma, Toast.LENGTH_SHORT).show();
                            } else {
                                Bitmap bmp = holder.sView.getTransparentSignatureBitmap();
                                holder.sView.clear();
                                processFirmaAsync(bmp);
                            }
                        }
                    });
                }
            });
        }

        return signatureDialog;
    }

    public boolean validar(EditText nombre, EditText email, EditText pan) {
        if (TextUtils.isEmpty(nombre.getText())) {
            nombre.requestFocus();
            nombre.setError(mContext.getText(R.string.error_pago_nombre));

            return false;
        }

        if (TextUtils.isEmpty(email.getText())) {
            email.requestFocus();
            email.setError(res.getText(R.string.error_pago_email));

            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches()) {
            email.requestFocus();
            email.setError(res.getText(R.string.error_pago_email_valido));

            return false;
        }

        if (tarjeta == null) {
            if (TextUtils.isEmpty(pan.getText()) || !TextUtils.isDigitsOnly(pan.getText())) {
                pan.requestFocus();
                pan.setError(res.getText(R.string.error_pago_pan));

                return false;
            }
        }

        return true;
    }

    private String processFirma(Bitmap bmp) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2056);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();

            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (Exception e) {
            Log.e("PagoPresenter", "processFirma", e);
        }

        return null;
    }

    public void processFirmaAsync(final Bitmap bmp) {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                pDialog.setMessage(res.getText(R.string.progress_pago_firma));
                pDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                return processFirma(bmp);
            }

            @Override
            protected void onPostExecute(String s) {
                pDialog.dismiss();
                
                if (TextUtils.isEmpty(s)) {
                    mCallback.onError(new DefaultResponse(-9999, res.getString(R.string.error_addcel_default)));
                } else {
                    mCallback.onFirmaProcessed(s);
                }

            }
        }.execute();
    }


    public void getTokenAsync() {
        new AsyncTask<Void, Void, TokenResponse>() {
            @Override
            protected void onPreExecute() {
                pDialog.setMessage(res.getText(R.string.progress_pago_token));
                pDialog.show();
            }

            @Override
            protected TokenResponse doInBackground(Void... params) {
                return client.getToken();
            }

            @Override
            protected void onPostExecute(TokenResponse tokenResponse) {
                pDialog.dismiss();
                if (tokenResponse == null)
                    mCallback.onError(
                            new DefaultResponse(
                                    -10000, res.getString(R.string.error_addcel_default)));
                else {
                    if (tokenResponse.getIdError() != 0)
                        mCallback.onError(tokenResponse);
                    else
                        mCallback.onTokenReceived(tokenResponse);
                }
            }
        }.execute();
    }

    static class ViewHolder {

        @Bind(R.id.view_firma)
        SignaturePad sView;

        public ViewHolder(View aux) {
            ButterKnife.bind(this, aux);
        }


    }
}
