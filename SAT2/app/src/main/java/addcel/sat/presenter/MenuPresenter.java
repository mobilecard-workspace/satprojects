package addcel.sat.presenter;

import android.content.Context;
import android.widget.ArrayAdapter;

import addcel.sat.R;

/**
 * Created by carlosgs on 05/09/15.
 */
public class MenuPresenter {

    private Context mContext;
    private ArrayAdapter<String > mAdapter;

    public MenuPresenter(Context context) {
        mContext = context;
        mAdapter = new ArrayAdapter<>(
                mContext, R.layout.view_menu,
                mContext.getResources().getStringArray(R.array.arr_menu));
    }

    public ArrayAdapter<String> getmAdapter() {
        return mAdapter;
    }
}
