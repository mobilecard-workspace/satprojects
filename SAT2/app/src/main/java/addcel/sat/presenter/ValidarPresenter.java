package addcel.sat.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.text.TextUtils;

import org.addcel.android.crypto.Cifrados;

import java.util.Calendar;

import addcel.sat.R;
import addcel.sat.Url;
import addcel.sat.api.SatClient;
import addcel.sat.api.SatClientFactory;
import addcel.sat.callback.ValidarCallback;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.ConsultaResponse;
import addcel.sat.to.response.DefaultResponse;
import addcel.util.AddcelClient;
import addcel.util.AddcelFactory;

/**
 * Created by carlosgs on 05/09/15.
 */
public class ValidarPresenter {

    protected Context mContext;
    protected ValidarCallback mCallback;
    private ProgressDialog pDialog;
    private Resources res;
    private SatClient client = SatClientFactory.get(true);

    public ValidarPresenter(Context context, ValidarCallback callback) {
        mContext = context;
        mCallback = callback;
        res = mContext.getResources();
        pDialog = AddcelFactory.getpDialog(mContext);
    }

    public Context getmContext() {
        return mContext;
    }

    public ValidarCallback getmCallback() {
        return mCallback;
    }

    public Resources getRes() {
        return res;
    }

    public void validaLineaAsync(final String linea) {
        String[] arr = TextUtils.split(linea, "\\s");
        String lc = arr[0];
        double monto = Double.parseDouble(arr[1]);

        validaLineaAsync(lc, monto);
    }

    public void validaLineaAsync(final String lc, final double monto) {
        new AsyncTask<Void, Void, PagoData>() {

            @Override
            protected void onPreExecute() {
                pDialog.setMessage(res.getText(R.string.progress_lc_valida));
                pDialog.show();
            }

            @Override
            protected PagoData doInBackground(Void... params) {
                return client.validaLinea(lc, monto);
            }

            @Override
            protected void onPostExecute(PagoData consultaResponse) {
                pDialog.dismiss();
                if (consultaResponse == null)
                    mCallback.onError(new DefaultResponse(-1000, res.getString(R.string.error_addcel_default)));
                else {
                    switch (consultaResponse.getIdError()) {
                        case 0:
                            mCallback.onLineaValida(consultaResponse);
                            break;
                        default:
                            mCallback.onError(consultaResponse);
                            break;
                    }
                }
            }
        }.execute();
    }

    public void consultaLineaAsync(final String lc) {
        new AsyncTask<Void, Void, PagoData>() {

            @Override
            protected void onPreExecute() {
                pDialog.setMessage(res.getText(R.string.progress_consultar_lc));
                pDialog.show();
            }

            @Override
            protected PagoData doInBackground(Void... params) {
                return client.consultar(lc);
            }

            @Override
            protected void onPostExecute(PagoData consultaResponse) {
                pDialog.dismiss();
                if (consultaResponse == null)
                    mCallback.onError(new DefaultResponse(-1000, res.getString(R.string.error_addcel_default)));
                else {
                    switch (consultaResponse.getIdError()) {
                        case 0:
                            mCallback.onLineaConsultada(consultaResponse);
                            break;
                        default:
                            mCallback.onError(consultaResponse);
                            break;
                    }
                }
            }
        }.execute();
    }
}
