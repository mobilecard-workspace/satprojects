package addcel.sat.presenter;

import android.content.Context;
import android.os.Handler;

import java.util.concurrent.TimeUnit;

import addcel.sat.callback.LaunchCallback;

/**
 * Created by carlosgs on 04/09/15.
 */
public class LaunchPresenter {

    private Context mContext;
    private LaunchCallback mCallback;

    private static final int VERSION_WAIT = 5;

    public LaunchPresenter(Context mContext, LaunchCallback callback) {
        this.mContext = mContext;
        this.mCallback = callback;
    }

    public void swapSplash() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mCallback.onSplashSwaped();
            }
        }, TimeUnit.SECONDS.toMillis(2));

    }

    public void launchGetVersion() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getVersion();
            }
        }, TimeUnit.SECONDS.toMillis(VERSION_WAIT));
    }

    private void getVersion() {
        mCallback.onVersionVerified();
    }
}
