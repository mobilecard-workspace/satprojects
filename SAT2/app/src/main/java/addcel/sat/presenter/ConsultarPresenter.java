package addcel.sat.presenter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;

import java.util.Calendar;
import java.util.List;

import addcel.sat.CameraActivity;
import addcel.sat.ConsultarActivity;
import addcel.sat.R;
import addcel.sat.api.SatClient;
import addcel.sat.api.SatClientFactory;
import addcel.sat.callback.ConsultarCallback;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.DefaultResponse;
import addcel.util.AddcelFactory;
import addcel.util.AddcelTextUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by carlosgs on 14/09/15.
 */
public class ConsultarPresenter implements ConsultarCallback{

    @Bind(R.id.text_consultar_captura)
    EditText lcText;

    @Bind(R.id.view_consultar_lc)
    TextView lcView;
    @Bind(R.id.view_consultar_monto)
    TextView montoView;
    @Bind(R.id.view_consultar_email)
    TextView emailView;
    @Bind(R.id.view_consultar_auth)
    TextView authView;
    @Bind(R.id.view_consultar_fecha)
    TextView fechaView;

    @Bind({R.id.view_consultar_lc, R.id.view_consultar_monto, R.id.view_consultar_email,
            R.id.view_consultar_auth, R.id.view_consultar_fecha, R.id.b_consultar_reenviar})
    List<TextView> consultaViews;

    private Context mContext;
    private AlertDialog mScanDialog;
    private SatClient mClient = SatClientFactory.get(true);
    private ProgressDialog pDialog;
    private Resources res;
    private PagoData mPagoData;

    private static final String TAG = "ConsultarPresenter";

    public ConsultarPresenter(Context context) {
        mContext = context;
        pDialog = AddcelFactory.getpDialog(mContext);
        res = mContext.getResources();
        ButterKnife.bind(this, (ConsultarActivity) mContext);
    }

    public AlertDialog getmScanDialog() {

        if (mScanDialog == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                    .setSingleChoiceItems(R.array.arr_consulta, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                            Intent intent = new Intent();

                            switch (which) {
                                case 0:
                                    intent.setClass(
                                            mContext, CameraActivity.class)
                                            .putExtra("format", BarcodeFormat.QR_CODE);
                                    break;
                                case 1:
                                    intent.setClass(
                                            mContext, CameraActivity.class)
                                            .putExtra("format", BarcodeFormat.EAN_13);
                                    break;
                                default:
                                    break;
                            }

                            intent.putExtra("isConsulta", true);
                            ((ConsultarActivity) mContext).startActivityForResult(intent, 1);
                        }
                    });

            mScanDialog = builder.create();

        }

        return mScanDialog;
    }

    private boolean validate() {
        CharSequence lc = lcText.getText();

        if (TextUtils.isEmpty(lc)) {
            lcText.requestFocus();
            lcText.setError(mContext.getText(R.string.error_lc_captura_vacia));
            return false;
        }

        return true;
    }

    @OnClick(R.id.b_consultar_scan)
    void scan() {
        getmScanDialog().show();
    }

    @OnClick(R.id.b_consultar_consultar)
    void consultar() {
        if (validate()) {
            consultarAsync(lcText.getText().toString().trim());
        }
    }

    private void consultarAsync(final String lc) {

        new AsyncTask<Void, Void, PagoData>() {

            @Override
            protected void onPreExecute() {
                pDialog.setMessage(res.getString(R.string.progress_consultar_lc));
                pDialog.show();
            }

            @Override
            protected PagoData doInBackground(Void... params) {
                return mClient.consultar(lc);
            }

            @Override
            protected void onPostExecute(PagoData pagoData) {
                pDialog.dismiss();
                if (pagoData == null)
                    onError(new DefaultResponse(-10000, res.getString(R.string.error_addcel_default)));
                else {
                    if (pagoData.getIdError() != 0)
                        onError(pagoData);
                    else
                        paintConsultaResult(pagoData);
                }
            }
        }.execute();
    }

    @Override
    public void paintConsultaResult(PagoData data) {
        Log.d(TAG, "paintConsultaResult");
        if (data == null) {
            TextKeyListener.clear(lcText.getText());
            ButterKnife.apply(consultaViews, HIDE);

        } else {

            mPagoData = data;

            lcText.setText(data.getLincap());
            lcView.setText(
                    AddcelTextUtils.getLabelText(
                            mContext, R.string.txt_consultar_lc, "consulta_lc", data.getLincap()));
            montoView.setText(
                    AddcelTextUtils.getLabelText(
                            mContext, R.string.txt_consultar_monto, "consulta_monto", AddcelTextUtils.getCurrencyString(data.getMonto())));
            authView.setText(
                    AddcelTextUtils.getLabelText(
                            mContext, R.string.txt_consultar_auth, "consulta_auth", "00000000"));
            emailView.setText(
                    AddcelTextUtils.getLabelText(
                            mContext, R.string.txt_consultar_email, "consulta_email", data.getEmail()));
            fechaView.setText(
                    AddcelTextUtils.getLabelText(
                            mContext, R.string.txt_consultar_fecha, "consulta_fecha", AddcelTextUtils.getDateString(Calendar.getInstance().getTimeInMillis())));

            ButterKnife.apply(consultaViews, SHOW);
        }
    }

    @Override
    public void onError(DefaultResponse error) {
        Log.d(TAG, "onError");
        mPagoData = null;
        ButterKnife.apply(consultaViews, HIDE);
        Toast.makeText(mContext, error.toString(), Toast.LENGTH_SHORT).show();
    }

    static ButterKnife.Action<TextView> SHOW = new ButterKnife.Action<TextView>() {
        @Override
        public void apply(TextView view, int index) {
            view.setVisibility(View.VISIBLE);
        }
    };

    static ButterKnife.Action<TextView> HIDE = new ButterKnife.Action<TextView>() {
        @Override
        public void apply(TextView view, int index) {

            if(!(view instanceof Button))
                view.setText("");

            view.setVisibility(View.GONE);
        }
    };
}
