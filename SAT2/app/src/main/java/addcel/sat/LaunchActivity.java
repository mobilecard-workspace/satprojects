package addcel.sat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import addcel.sat.callback.LaunchCallback;
import addcel.sat.presenter.LaunchPresenter;
import butterknife.Bind;
import butterknife.BindDrawable;
import butterknife.ButterKnife;

public class LaunchActivity extends Activity implements LaunchCallback {

    @Bind(R.id.splash)
    ImageView splash;

    @BindDrawable(R.drawable.t_bienvenido)
    Drawable subSplash;

    @Bind({R.id.header, R.id.footer})
    List<View> borderViews;

    private LaunchPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        ButterKnife.bind(this);

        presenter = new LaunchPresenter(this, this);

        presenter.swapSplash();
    }

    @Override
    public void onSplashSwaped() {
        splash.setScaleType(ImageView.ScaleType.CENTER);
        splash.setImageDrawable(subSplash);

        ButterKnife.apply(borderViews, SHOW);
        presenter.launchGetVersion();
    }

    @Override
    public void onVersionVerified() {
        startActivity(new Intent(this, MenuActivity.class));
    }

    public static ButterKnife.Action<View> SHOW = new ButterKnife.Action<View>() {
        @Override
        public void apply(View view, int index) {
            view.setVisibility(View.VISIBLE);
        }
    };
}
