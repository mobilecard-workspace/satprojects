package addcel.sat;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.util.Calendar;

import addcel.sat.callback.PagoCallback;
import addcel.sat.presenter.PagoPresenter;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.ConsultaResponse;
import addcel.sat.to.response.DefaultResponse;
import addcel.sat.to.response.TokenResponse;
import addcel.util.AddcelFactory;
import addcel.util.AddcelTextUtils;
import addcel.util.DeviceUtils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class DatosPagoActivity extends Activity implements PagoCallback{

    @Bind(R.id.view_pago_lc)
    TextView lcView;
    @Bind(R.id.view_pago_monto)
    TextView montoView;
    @Bind(R.id.text_pago_nombre)
    EditText nombreText;
    @Bind(R.id.text_pago_email)
    EditText emailText;
    @Bind(R.id.text_pago_tarjeta)
    EditText tarjetaText;


    private PagoPresenter presenter;

    private static final String TAG = "DatosPagoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_pago);
        ButterKnife.bind(this);

        PagoData lc = Parcels.unwrap(getIntent().getExtras().getParcelable("consulta"));
        presenter = new PagoPresenter(this, this, lc);

        lcView.setText(presenter.getmData().getLincap());
        montoView.setText(AddcelTextUtils.getCurrencyString(presenter.getmData().getImporte()));
    }

    @Override
    public void onError(DefaultResponse error) {

        if (error.getIdError() == -9999) {
            presenter.getSignatureDialog().dismiss();
        }

        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.b_pago_scan)
    void getCardData() {

        presenter.setTarjeta(null);

        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, PagoPresenter.SCAN_REQUEST);
    }

    @OnClick(R.id.b_pago_pagar)
    void pagar() {
        if (presenter.validar(nombreText, emailText, tarjetaText)) {

            if (presenter.getTarjeta() == null) {
                presenter.setTarjeta(new CreditCard(tarjetaText.getText().toString().trim(), 1, Calendar.getInstance().get(Calendar.YEAR), "", ""));
            }

            presenter.getmData().setNombre(nombreText.getText().toString().trim());
            presenter.getmData().setEmail(emailText.getText().toString().trim());

            presenter.getSignatureDialog().show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

            lp.copyFrom(presenter.getSignatureDialog().getWindow().getAttributes());
            lp.width = DeviceUtils.getWidth(this);
            int height = DeviceUtils.getHeight(this);

            Log.d(TAG, "Altura dispositivo: " + height);

            if (height >= DeviceUtils.TABLET_HEIGHT)
                lp.height = height - height / 4;
            else
                lp.height = height - height / 8;

            presenter.getSignatureDialog().getWindow().setAttributes(lp);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PagoPresenter.SCAN_REQUEST) {

            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                presenter.setTarjeta(scanResult);
                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                tarjetaText.setText(scanResult.getRedactedCardNumber());
            }
            else {
                presenter.setTarjeta(null);
                onError(new DefaultResponse(1000, getString(R.string.error_pago_tarjeta)));
            }
        }
    }

    @Override
    public void onFirmaProcessed(String firma) {

        presenter.getSignatureDialog().dismiss();

        presenter.getmData().setImei(DeviceUtils.getIMEI(this));
        presenter.getmData().setCard(presenter.getTarjeta());

        Log.d("PagoPresenter", AddcelFactory.getGson().toJson(presenter.getmData()));
        presenter.getmData().setFirma(firma);

        presenter.getTokenAsync();
    }

    @Override
    public void onTokenReceived(TokenResponse token) {

        presenter.getmData().setToken(token.getToken());

        Bundle bundle = new Bundle();
        bundle.putParcelable("datos", Parcels.wrap(PagoData.class, presenter.getmData()));

        Intent intent = new Intent(this, SecureActivity.class).putExtras(bundle);
        startActivity(intent);
    }
}
