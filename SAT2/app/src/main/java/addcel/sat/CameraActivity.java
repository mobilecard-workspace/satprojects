package addcel.sat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import org.parceler.Parcels;

import addcel.sat.callback.CameraCallback;
import addcel.sat.presenter.CameraPresenter;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.ConsultaResponse;
import addcel.sat.to.response.DefaultResponse;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class CameraActivity extends Activity implements CameraCallback {

    private ZXingScannerView scannerView;
    private CameraPresenter presenter;
    private boolean consulta = false;

    private static final String TAG = "CameraActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);

        BarcodeFormat format = (BarcodeFormat) getIntent().getSerializableExtra("format");
        consulta = getIntent().getBooleanExtra("isConsulta", false);

        presenter = new CameraPresenter(
                this, this, format);
    }


    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        scannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        presenter.validarCodigo(rawResult);

    }

    @Override
    public void onValidCode(String code) {
        Log.d(TAG, "onValidCode");
        Log.d(TAG, "Value of 'consulta': " + consulta);

        if (consulta) {
            String arr[] = TextUtils.split(code, "\\s");
            presenter.consultaLineaAsync(arr[0]);
        } else
            presenter.validaLineaAsync(code);
    }

    @Override
    public void onLineaValida(PagoData response) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("consulta", Parcels.wrap(PagoData.class, response));
        Intent intent = new Intent(this, DatosPagoActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onLineaConsultada(PagoData response) {
        Log.d(TAG, "onLineaConsultada");
        Bundle bundle = new Bundle();
        bundle.putParcelable("consulta", Parcels.wrap(PagoData.class, response));

        Intent returnIntent = new Intent();
        returnIntent.putExtras(bundle);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onError(DefaultResponse error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        if (consulta)
            setResult(RESULT_CANCELED);

        finish();
    }
}
