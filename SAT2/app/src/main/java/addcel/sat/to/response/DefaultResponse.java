package addcel.sat.to.response;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

/**
 * Created by carlosgs on 05/09/15.
 */

@Parcel
public class DefaultResponse {


//    @Expose
    int idError;

//    @Expose
    String mensajeError;

    public DefaultResponse() {
    }

    public DefaultResponse(int idError, String mensajeError) {
        this();
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    @Override
    public String toString() {
        return mensajeError;
    }
}
