package addcel.sat.to;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import addcel.sat.to.response.ConsultaResponse;
import io.card.payment.CreditCard;

/**
 * Created by carlosgs on 07/09/15.
 */

@Parcel
public class PagoData extends ConsultaResponse {

    CreditCard card;
    String nombre;
    @Expose
    String email;
    @Expose
    @SerializedName("firmaBase64")
    String firma;
    String imei;
    String tipo;
    String software;
    String modelo;
    String token;

    //PROVISIONALES
    @Expose
    double monto;
    @Expose
    String lineaDeCapturaSat;

    /*
    {
                         I      "lineaDeCapturaSat": "01140G5W110097645297",
                         I      "email": "elmans8@gmail.com",
                         I      "firmaBase64": "",
                         I      "monto": 1500
                         I  }
     */

    public PagoData() {
        super();
    }

    public PagoData(int idError, String msgError) {
        super(idError, msgError);
    }

    public PagoData(CreditCard card, String nombre, String email, String firma) {
        super();
        this.card = card;
        this.nombre = nombre;
        this.email = email;
        this.firma = firma;
    }

    public CreditCard getCard() {
        return card;
    }

    public void setCard(CreditCard card) {
        this.card = card;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    //PROVISIONAL

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getLineaDeCapturaSat() {
        return lineaDeCapturaSat;
    }

    public void setLineaDeCapturaSat(String lineaDeCapturaSat) {
        this.lineaDeCapturaSat = lineaDeCapturaSat;
    }
}
