package addcel.sat.to.response;

import org.parceler.Parcel;

/**
 * Created by carlosgs on 10/09/15.
 */

@Parcel
public class TokenResponse extends DefaultResponse{

    String token;

    public TokenResponse() {
        super();
    }

    public TokenResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
