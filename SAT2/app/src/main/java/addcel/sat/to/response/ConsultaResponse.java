package addcel.sat.to.response;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

/**
 * Created by carlosgs on 09/09/15.
 */

@Parcel
public class ConsultaResponse extends DefaultResponse {

//    @Expose
    String tranid;
//    @Expose
    String referen;
//    @Expose
    long fecha;
//    @Expose
    String hora;
//    @Expose
    int hora01;
//    @Expose
    int centro;
//    @Expose
    String lincap;
//    @Expose
    String llavpgo;
//    @Expose
    String usuario;
//    @Expose
    String estatus;
//    @Expose
    String plaza;
//    @Expose
    double importe;
//    @Expose
    String banda;

    public ConsultaResponse() {
        super();
    }

    public ConsultaResponse(int idError, String msgError) {
        super(idError, msgError);
    }

    public String getTranid() {
        return tranid;
    }

    public void setTranid(String tranid) {
        this.tranid = tranid;
    }

    public String getReferen() {
        return referen;
    }

    public void setReferen(String referen) {
        this.referen = referen;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getHora01() {
        return hora01;
    }

    public void setHora01(int hora01) {
        this.hora01 = hora01;
    }

    public int getCentro() {
        return centro;
    }

    public void setCentro(int centro) {
        this.centro = centro;
    }

    public String getLincap() {
        return lincap;
    }

    public void setLincap(String lincap) {
        this.lincap = lincap;
    }

    public String getLlavpgo() {
        return llavpgo;
    }

    public void setLlavpgo(String llavpgo) {
        this.llavpgo = llavpgo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public String getBanda() {
        return banda;
    }

    public void setBanda(String banda) {
        this.banda = banda;
    }
}
