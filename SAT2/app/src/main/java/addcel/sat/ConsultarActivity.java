package addcel.sat;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import org.parceler.Parcels;

import addcel.sat.presenter.ConsultarPresenter;
import addcel.sat.to.PagoData;

public class ConsultarActivity extends Activity {

    private ConsultarPresenter mPresenter;
    private static final String TAG = "ConsultarActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar);
        mPresenter = new ConsultarPresenter(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       if (resultCode == RESULT_OK) {
           Log.d(TAG, "onActivityResult");
           PagoData pData = Parcels.unwrap(data.getExtras().getParcelable("consulta"));
           mPresenter.paintConsultaResult(pData);
       } else {
           mPresenter.paintConsultaResult(null);
       }
    }
}
