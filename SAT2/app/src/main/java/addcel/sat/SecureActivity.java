package addcel.sat;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import org.parceler.Parcels;

import addcel.sat.presenter.SecurePresenter;
import addcel.sat.to.PagoData;
import butterknife.ButterKnife;

public class SecureActivity extends Activity {

    private SecurePresenter presenter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_secure);

        PagoData data = Parcels.unwrap(getIntent().getExtras().getParcelable("datos"));
        presenter = new SecurePresenter(this, data);
        presenter.initTransaction();
    }

    @Override
    public void onBackPressed() {
       presenter.onBackPressed();
    }

    public void superOnBackPressed() {
        super.onBackPressed();
    }
}
