package addcel.sat.callback;

import addcel.sat.to.PagoData;
import addcel.sat.to.response.ConsultaResponse;
import addcel.sat.to.response.DefaultResponse;

/**
 * Created by carlosgs on 05/09/15.
 */
public interface ValidarCallback extends DefaultCallback {

    public void onLineaValida(PagoData response);
    public void onLineaConsultada(PagoData response);
}
