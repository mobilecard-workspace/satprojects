package addcel.sat.callback;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by carlosgs on 05/09/15.
 */
public interface CameraCallback extends ValidarCallback, ZXingScannerView.ResultHandler {

    public void onValidCode(String code);

}
