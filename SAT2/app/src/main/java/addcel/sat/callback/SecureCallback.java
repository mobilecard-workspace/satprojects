package addcel.sat.callback;

/**
 * Created by carlosgs on 08/09/15.
 */
public interface SecureCallback {

    public void processHTML(String html);
}
