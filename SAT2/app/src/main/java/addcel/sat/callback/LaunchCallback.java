package addcel.sat.callback;

/**
 * Created by carlosgs on 05/09/15.
 */
public interface LaunchCallback {

    public void onSplashSwaped();
    public void onVersionVerified();
}
