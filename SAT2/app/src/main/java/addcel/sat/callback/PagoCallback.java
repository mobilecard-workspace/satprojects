package addcel.sat.callback;

import addcel.sat.to.response.TokenResponse;

/**
 * Created by carlosgs on 05/09/15.
 */
public interface PagoCallback extends DefaultCallback {

    public void onFirmaProcessed(String firma);
    public void onTokenReceived(TokenResponse token);

}
