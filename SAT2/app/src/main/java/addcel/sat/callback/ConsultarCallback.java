package addcel.sat.callback;

import addcel.sat.to.PagoData;

/**
 * Created by carlosgs on 15/09/15.
 */
public interface ConsultarCallback extends DefaultCallback {
    
    void paintConsultaResult(PagoData data);
}
