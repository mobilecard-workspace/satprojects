package addcel.sat.callback;

import addcel.sat.to.response.DefaultResponse;

/**
 * Created by carlosgs on 05/09/15.
 */
public interface DefaultCallback {

    public void onError(DefaultResponse error);
}
