package addcel.sat.api;

import addcel.sat.to.PagoData;
import addcel.sat.to.response.TokenResponse;

/**
 * Created by carlosgs on 10/09/15.
 */
public interface SatClient {

    public PagoData validaLinea(String lc, double monto);
    public TokenResponse getToken();
    public PagoData consultar(String lc);

}
