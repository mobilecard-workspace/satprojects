package addcel.sat.api;

import android.text.TextUtils;

import addcel.sat.to.PagoData;
import addcel.sat.to.response.TokenResponse;

/**
 * Created by carlosgs on 10/09/15.
 */
public class SatClientMockImpl implements SatClient {

    private static final String txt_lc_valida = "01140G5W110097645297 1500";
    private static final String txt_lc_valida_consulta = "01140G5W110097645297";
    private static final String txt_lc_incorrecto="01140G5W112097645297 2510";
    private static final String txt_lc_incorrecto_consulta = "01140G5W112097645297";
    private static final String error_lc_incorrecto="El monto a pagar no es válido.";
    private static final String txt_lc_extemporanea="01140G5W111097645297 1500";
    private static final String getTxt_lc_extemporanea_consulta = "01140G5W111097645297";
    private static final String error_lc_extemporanea="El periodo de pago de esta línea de captura ha expirado.";
    private static final String txt_lc_malformada="01140G5W1100976452977 1500";
    private static final String txt_lc_malformada_consulta = "01140G5W1100976452977";
    private static final String error_lc_malformada="El formato de esta línea de captura no es válido.";

    public SatClientMockImpl() {
    }

    @Override
    public PagoData validaLinea(String lc, double monto) {
        String linea = lc + " " + ((int) monto);

        PagoData c = new PagoData(4, error_lc_incorrecto);

        if (TextUtils.equals(txt_lc_valida, linea)) {
            c = new PagoData(0, txt_lc_valida);
           c.setLincap(lc);
            c.setImporte(monto);
            c.setMonto(monto);
            c.setLineaDeCapturaSat(lc);

            return c;

        }

        if (TextUtils.equals(txt_lc_incorrecto,linea)) {
            c = new PagoData(1, error_lc_incorrecto);
            return c;
        }

        if (TextUtils.equals(txt_lc_extemporanea,linea)) {
            c = new PagoData(2, error_lc_extemporanea);
            return c;
        }

        if (TextUtils.equals(txt_lc_malformada,linea)) {
            c = new PagoData(3, error_lc_malformada);
            return c;
        }

        return c;
    }

    @Override
    public TokenResponse getToken() {

        TokenResponse response = new TokenResponse(0, "Éxito");
        response.setToken("xksjfykgsjysuyrf==");

        return response;
    }

    @Override
    public PagoData consultar(String lc) {

        PagoData c = new PagoData(4, error_lc_incorrecto);

        if (TextUtils.equals(txt_lc_valida_consulta, lc)) {
            c = new PagoData(0, txt_lc_valida);
            c.setLincap(lc);
            c.setImporte(1500);
            c.setMonto(1500);
            c.setEmail("elmans8@gmail.com");
            c.setLineaDeCapturaSat(lc);

            return c;

        }

        if (TextUtils.equals(txt_lc_incorrecto_consulta,lc)) {
            c = new PagoData(1, error_lc_incorrecto);
            return c;
        }

        if (TextUtils.equals(getTxt_lc_extemporanea_consulta,lc)) {
            c = new PagoData(2, error_lc_extemporanea);
            return c;
        }

        if (TextUtils.equals(txt_lc_malformada_consulta,lc)) {
            c = new PagoData(3, error_lc_malformada);
            return c;
        }

        return c;
    }
}
