package addcel.sat.api;

import android.text.TextUtils;

import org.addcel.android.crypto.Cifrados;

import addcel.sat.Url;
import addcel.sat.to.PagoData;
import addcel.sat.to.response.TokenResponse;
import addcel.util.AddcelClient;

/**
 * Created by carlosgs on 10/09/15.
 */
public final class SatClientImpl implements SatClient {

    public SatClientImpl() {
    }

    @Override
    public PagoData validaLinea(String lc, double monto) {
        String param = "{" +
                "\"importe\":"+monto+"," +
                " \"lincap\":\""+lc+"\"" +
                "}";

        return AddcelClient.get().post(Url.CONSULTA, param, Cifrados.HARD, PagoData.class);
    }

    @Override
    public TokenResponse getToken() {
        String param = "{\"usuario\":\"usuario\", \"password\":\"password\"}";

        return AddcelClient.get().post(Url.TOKEN, param, Cifrados.HARD, TokenResponse.class);
    }

    @Override
    public PagoData consultar(String lc) {
        return null;
    }
}
