package addcel.sat.api;

/**
 * Created by carlosgs on 10/09/15.
 */
public final class SatClientFactory {


    private static SatClient client;

    private SatClientFactory() {
    }

    /**
     *
     * @param mockClient - Si es {@code true}, la implementación es MOCK
     * @return Implementación de la interfaz SatClient
     */
    public static final synchronized SatClient get(boolean mockClient) {
        if (mockClient)
            client = new SatClientMockImpl();
        else
            client = new SatClientImpl();

        return client;
    }
}
