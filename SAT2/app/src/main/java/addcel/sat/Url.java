package addcel.sat;

/**
 * Created by carlosgs on 07/09/15.
 */
public final class Url {

    private Url() {
    }

    private static final String DEV = "http://50.57.192.214:8080/";
    private static final String PROD = "https://www.mobilecard.mx:8443/";

    private static final String BASE = DEV + "SATServicios/";

    public static final String CONSULTA = BASE + "consulta";
    public static final String TOKEN = BASE + "getToken";
    public static final String PAGO = BASE + "inicio";
}
