package addcel.sat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.google.zxing.BarcodeFormat;

import addcel.sat.presenter.MenuPresenter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MenuActivity extends Activity {

    @Bind(R.id.list_menu)
    ListView menuList;

    private MenuPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        presenter = new MenuPresenter(this);
        menuList.setAdapter(presenter.getmAdapter());
    }


    @OnItemClick(R.id.list_menu)
    void pickOption(int position) {

        Intent intent = new Intent();

        switch (position) {
            case 0:
                intent.setClass(this, CameraActivity.class).putExtra("format", BarcodeFormat.QR_CODE);
                break;
            case 1:
                intent.setClass(this, CameraActivity.class).putExtra("format", BarcodeFormat.EAN_13);
                break;
            case 2:
                intent.setClass(this, ValidarActivity.class);
                break;
            case 3:
                intent.setClass(this, ConsultarActivity.class);
                break;
        }

            startActivity(intent);

    }
}
