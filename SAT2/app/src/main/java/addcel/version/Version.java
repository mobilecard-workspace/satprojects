package addcel.version;

import com.google.gson.annotations.Expose;

import addcel.util.AddcelFactory;


public class Version {

    @Expose
    private int id;
    @Expose
    private String version;
    @Expose
    private String url;
    @Expose
    private int tipo;

    public Version() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
