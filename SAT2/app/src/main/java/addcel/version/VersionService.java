package addcel.version;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface VersionService {

    @GET("Vitamedica/getVersion")
    Call<Version> getVersion(@Query("idPlataforma")String idPlataforma, @Query("idApp")String idApp);

}
